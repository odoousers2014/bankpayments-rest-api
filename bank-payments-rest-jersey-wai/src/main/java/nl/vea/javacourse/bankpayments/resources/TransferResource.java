package nl.vea.javacourse.bankpayments.resources;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.responsehandling.ResourceNotFoundException;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.service.TransferService;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.springframework.stereotype.Component;

@Component
@Path("/transfers")
public class TransferResource {

	@Inject
	TransferService transferService;
	
	public TransferResource() {
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("by-bank-account-nr/{accountNumber}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<TransferVO> getTransfersByBankAccount(@PathParam("accountNumber") Integer accountNumber){
		return TransferVO.getVOfromEntities(transferService.findAllByAccountNumber( accountNumber));
	}
	
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public TransferVO getTransferById(@PathParam("id") Long id) throws ResourceNotFoundException{
		Transfer result = transferService.findById(id);
		if (result == null) {
			throw new ResourceNotFoundException("Transfer", "transferId", id.toString());
		}
		return new TransferVO(transferService.findById(id));
	}
	
	//method to test behavior of UriInfo and UriBuilder
	@GET
	@Path("echo-path/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getPathInfo(@Context HttpServletRequest servletRequest, @Context UriInfo uriInfo, @PathParam("id") Long id) throws ResourceNotFoundException{
		URI echoPath = uriInfo.getAbsolutePath();
		URI resourcePath = UriBuilder.fromResource(BankAccountResource.class).path(BankAccountResource.class, "getBankAccountById").build("5");		
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.OK)
				// 201
				.entity(new ResponseMessage(Response.Status.OK, echoPath.toString(), "absolute URI for bank-account with id = 5: "+location))
				.contentLocation(echoPath)
				.header("location", echoPath)
				.build();
	}
}
