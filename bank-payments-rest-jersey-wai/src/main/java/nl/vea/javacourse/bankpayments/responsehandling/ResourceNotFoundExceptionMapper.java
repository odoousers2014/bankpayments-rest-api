package nl.vea.javacourse.bankpayments.responsehandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResourceNotFoundExceptionMapper implements
		ExceptionMapper<ResourceNotFoundException> {

	public ResourceNotFoundExceptionMapper() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Response toResponse(ResourceNotFoundException exception) {
		// see http://www.restpatterns.org/HTTP_Status_Codes/404_-_Not_Found
		return Response
				.status(Response.Status.NOT_FOUND)
				.entity(new ErrorMessage(
						Response.Status.NOT_FOUND,
						exception,
						String.format(
								"Check whether the value %s of %s within the url is correct",
								exception.getSearchAttributeValue(), exception.getSearchAttributeName()),
						String.format(
								"Verify the existence of the %s with the attribute %s with value %s in the database",
								exception.getEntityName(), exception.getSearchAttributeName(), exception.getSearchAttributeValue())))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
