package nl.vea.javacourse.bankpayments.resources;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.responsehandling.ResourceNotFoundException;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.responsehandling.UnauthorizedException;
import nl.vea.javacourse.bankpayments.service.BankAccountService;
import nl.vea.javacourse.bankpayments.vo.BankAccountVO;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/bank-accounts")
public class BankAccountResource {
	private static final Logger LOG = LoggerFactory.getLogger(BankAccountResource.class);

	@Autowired
	BankAccountService bankAccountService;

	public BankAccountResource() {
	}

	@POST
	@Path("create-for-customer")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.TEXT_HTML })
	public Response createBankAccountForExistingCustomer(
			@Context UriInfo uriInfo,
			@Context HttpServletRequest servletRequest,
			@FormParam("customer-id") Long customerId) throws IOException {
		BankAccount bankAccount = bankAccountService
				.createNewAccountFor(customerId);
		
		URI resourcePath = UriBuilder.fromResource(BankAccountResource.class).path(BankAccountResource.class, "getBankAccountById").build(String.valueOf(bankAccount.getAccountId()));
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location, "The transfer has successfully completed."))
				.contentLocation(resourcePath)
				.header("location", location)
				.build();
	}

	@POST
	@Path("transfer")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createTransferBetweenAccounts(@Context UriInfo uriInfo,
			@Context HttpServletRequest servletRequest,
			TransferVO transfer) throws BankAccountNotFoundException, OverdraftException, UnauthorizedException {
		//beware the TransferVO is only partially filled
		final String userName = servletRequest.getUserPrincipal().getName();
		LOG.debug(String.format("The name of the User Principal is %s", userName));
		BankAccount donor = bankAccountService.findByAccountNumber(transfer.getDonorAccountNumber());
		if(donor == null){
			throw new BankAccountNotFoundException(transfer.getDonorAccountNumber());
		}
		String accountUserName = donor.getCustomer().getAccountUserName();
		if(!userName.equals(accountUserName)){
			LOG.error(String.format("The user principal name is '%s' and the account user name of the corresponding customer in the database is '%s'", userName, accountUserName));
			throw new UnauthorizedException(userName, transfer.getDonorAccountNumber());
		}
		BankAccount beneficiary = bankAccountService.findByAccountNumber(transfer.getBeneficiaryAccountNumber());
		if(beneficiary == null){
			throw new BankAccountNotFoundException(transfer.getBeneficiaryAccountNumber());
		}
		Transfer result = bankAccountService.transfer(transfer.getAmount(), donor, beneficiary, transfer.getDescription());

		URI resourcePath = UriBuilder.fromResource(TransferResource.class).path(TransferResource.class, "getTransferById").build(String.valueOf(result.getTransferId()));		
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location, "The transfer has successfully completed."))
				.contentLocation(resourcePath)
				.header("location", location)
				.build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BankAccountVO> getBankAccounts() throws IOException {
		return BankAccountVO.getVOfromEntities(bankAccountService.findAll());
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BankAccountVO getBankAccountById(@PathParam("id") Long id)
			throws IOException, ResourceNotFoundException  {
		BankAccount bankAccount = bankAccountService.findById(id);
		if (bankAccount == null) {
			throw new ResourceNotFoundException("bank-account", "bank-account-id", id.toString());
		}
		return new BankAccountVO(bankAccount);
	}

	@GET
	@Path("account-nr/{account-nr}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BankAccountVO getBankAccountByAccountNumber(
			@PathParam("account-nr") Integer accountNumber) throws IOException, ResourceNotFoundException {
		BankAccount bankAccount = bankAccountService
				.findByAccountNumber(accountNumber);
		if (bankAccount == null) {
			throw new ResourceNotFoundException("bank-account", "account-nr", accountNumber.toString());
		}
		return new BankAccountVO(bankAccount);
	}

}
