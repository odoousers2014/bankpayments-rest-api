package nl.vea.javacourse.bankpayments.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * This {@link WebApplicationInitializer} implementation is necessary, because
 * the current version of Jersey-spring 3 does not support a
 * {@link AnnotationConfigWebApplicationContext} as yet.
 * {@link org.glassfish.jersey.server.spring.SpringWebApplicationInitializer }
 * The RestPocSpringWebAppInitializer does the following:
 * <ol>
 * 	<li>Sets up a Spring {@link ContextLoaderListener}</li>
 * 	<li>Tells the Context loader the location of the Spring Java Config {@link SpringApplicationContext} that is to be used for Services</li>
 * 	<li>Registers the Jersey Servlet Container {@link ServletContainer} by providing it the {@link JerseyResourceConfig} as the class to use for Resource and Provider management</li>
 * </ol>
 * 
 * @see <a
 *      href="http://sleeplessinslc.blogspot.nl/2014/04/jerseyjaxrs-2x-example-using-spring.html">http://sleeplessinslc.blogspot.nl/2014/04/jerseyjaxrs-2x-example-using-spring.html</a>
 *      <a
 *      href="https://java.net/jira/browse/JERSEY-2038">https://java.net/jira/
 *      browse/JERSEY-2038</a> <a href=
 *      "https://github.com/jersey/jersey/blob/master/ext/spring3/src/main/java/org/glassfish/jersey/server/spring/SpringWebApplicationInitializer.java?source=c#L63"
 *      >
 *      https://github.com/jersey/jersey/blob/master/ext/spring3/src/main/java/
 *      org/glassfish/jersey/server/spring/SpringWebApplicationInitializer.java?
 *      source=c#L63</a>
 * 
 *      <a href=
 *      "http://docs.spring.io/spring-framework/docs/current/javadoc-api/index.html?org/springframework/web/WebApplicationInitializer.html"
 *      >
 *      http://docs.spring.io/spring-framework/docs/current/javadoc-api/index.html
 *      ?org/springframework/web/WebApplicationInitializer.html at A 100%
 *      code-based approach to configuration</a> There it is also mentioned that
 *      "Remember that WebApplicationInitializer implementations are detected automatically -- so you are free to package them within your application as you see fit."
 * @author Willem
 *
 */
public class RestPocSpringWebAppInitializer implements
		WebApplicationInitializer {

	public RestPocSpringWebAppInitializer() {
	}

	/**
	 * {@inheritDoc} Note that the NotesApplicationInitializer has been set to
	 * highest precedence as that will ensure that it is executed before any
	 * other WebApplicationInitializer provided by accompanying jars. If for
	 * example, the SpringWebApplicationInitializer from jerseyspring3.jar gets
	 * loaded, then it attempts to find a spring applicationContext.xml and will
	 * fail as our example does not use spring xml style bean definitions.
	 * 
	 * @author Willem
	 */
	@Override
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public void onStartup(ServletContext container) throws ServletException {
		
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(AnnotationConfigWebApplicationContext.class);
        container.addListener(new ContextLoaderListener(rootContext));
        container.addListener(new RequestContextListener());
		
     // The following line is required to avoid having jersey-spring3 registering it's own Spring root context.
        container.setInitParameter(ContextLoader.CONFIG_LOCATION_PARAM, SpringApplicationContext.class.getName());


		// Register Jersey 2.x servlet (according to Servlet 2.x API)
		ServletRegistration.Dynamic servletRegistration = container.addServlet(
				"RestPoc", ServletContainer.class.getName());
		servletRegistration.addMapping("/*");
		servletRegistration.setLoadOnStartup(1);

		/*
		 * The name of the configuration property as defined by JAX-RS
		 * specification is indeed javax.ws.rs.Application and not
		 * javax.ws.rs.core.Application as one might expect.
		 * 
		 * @see <a href=
		 * "https://jersey.java.net/documentation/latest/deployment.html#deployment.servlet.2.application"
		 * >
		 * https://jersey.java.net/documentation/latest/deployment.html#deployment
		 * .servlet.2.application</a>
		 */
		servletRegistration.setInitParameter("javax.ws.rs.Application",
				JerseyResourceConfig.class.getName());
	}

}
