package nl.vea.javacourse.bankpayments.responsehandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;

@Provider
public class BankAccountNotFoundExceptionMapper implements
		ExceptionMapper<BankAccountNotFoundException> {

	public BankAccountNotFoundExceptionMapper() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Response toResponse(BankAccountNotFoundException exception) {
		// see http://www.restpatterns.org/HTTP_Status_Codes/409_-_Conflict
		return Response
				.status(Response.Status.CONFLICT)
				.entity(new ErrorMessage(
						Response.Status.CONFLICT,
						exception,
						"Check the account number.",
						"Invalid operation according to normal business rules; No error in programming, configuration or external systems"))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
