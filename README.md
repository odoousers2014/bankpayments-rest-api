# README #

## Introduction ##

This repository contains a maven project that serves as a proof of concept for creating REST services with the Jersey implementation of JAX-RS 2.0 in concert with Spring, Spring security, Spring Data JPA and the Hibernate JPA implementation.
It can be used to study and practice these techniques.

The business model is a simple collection of customers, which may have one or more bank accounts. Among these bank accounts money may be transferred. One can imagine REST services to 

* retrieve customers, bank accounts and money transfer records,
* create, update or delete customers and bank accounts,
* order a money transfer between a donor and a beneficiary account.

## Technology stack that is used in this example ##

*	Java SE 7.0
*	Maven 3
*	Oracle 11g XE DBMS
*	Oracle SQL Developer
*	Eclipse Luna
*	Spring 4.x
*	Spring Data 1.x
*   Spring Security 4.x
*	Hibernate 4.x implementing JPA2.1
*	Tomcat 8
*	Jersey 2.x implementing JAX_RS 2.0
*	Jackson 2.x


## How do I get set up? ##
Read the following installation manual to set up your development environment

[docs/InstallationManual_Maven_Tomcat8_Oracle+11g+XE.docx](https://gitlab.ing.net/Friday/bank-payments-rest-api/blob/master/docs/InstallationManual_Maven_Tomcat8_Oracle+11g+XE.docx)