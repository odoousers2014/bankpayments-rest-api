package nl.vea.javacourse.bankpayments.model;

import nl.vea.javacourse.bankpayments.OverdraftException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;


@NamedQueries({
	@NamedQuery(name = "BankAccount.findAll", query = "SELECT ba FROM BankAccount ba"),
	@NamedQuery(name = "BankAccount.findByAccountNumber", query = "SELECT ba FROM BankAccount ba WHERE ba.accountNumber = :accountNumber"),
	@NamedQuery(name = "BankAccount.findAllByCustomer", query = "SELECT ba FROM BankAccount ba WHERE ba.customer = :customer")})
@Entity
@Table(name = "BANK_ACCOUNTS")
public class BankAccount extends JsonEntity implements Serializable{

	private static final long serialVersionUID = 1771656573679790692L;
	private static final String OVERDRAFT_MSG = "The transfer of € %.2f from the account with id %s, number %s, balance € %.2f and creditLimit € %.2f, would lead to an overdraft of € %.2f";

	@Transient
	private final Logger logger = LoggerFactory.getLogger(BankAccount.class);

	@Id
	@SequenceGenerator(name = "ACCOUNTS_ACCOUNT_ID_GENERATOR", sequenceName = "ACCOUNTS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNTS_ACCOUNT_ID_GENERATOR")
	@Column(name = "ACCOUNT_ID", insertable = false, updatable = false)
	private long accountId;

	@Column(name = "ACCOUNT_NUMBER", nullable=false, length=10, scale=0, unique=true)
	private int accountNumber;
	
	@Column(name="BALANCE", nullable=false, length=10, scale=2)
	private BigDecimal balance;
	
	@Column(name="CREDIT_LIMIT", nullable=false, length=10, scale=2)
	private BigDecimal creditLimit;
	
	//BankAccount is the owning side
	//A new BankAccount can only be created in the database for an existing Customer
	//A BankAccount cannot be transferred from one Customer to another
	@ManyToOne
	@JoinColumn(name = "CUSTOMER_ID", insertable = true, updatable = false, nullable = false )
	private Customer customer;
	
	
	
	public BankAccount() {
		super();
	}

	public BankAccount(int accountNumber, BigDecimal balance,
			BigDecimal creditLimit, Customer customer) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.creditLimit = creditLimit;
		//Beware will lead to org.hibernate.LazyInitializationException if Customer has not initialized its list of accounts
		//Not necessary for BankAccount is the owning side
		//customer.addAccount(this);
		
		//instead simply set
		this.customer = customer;
	}
	
	public BankAccount(int accountNumber, BigDecimal balance,Customer customer) {
		this(accountNumber, balance,
				BigDecimal.valueOf(0), customer);
	}

	public BankAccount(int accountNumber, Customer customer) {
		this(accountNumber, BigDecimal.valueOf(0),
				BigDecimal.valueOf(0), customer);
	}

	
	//Used for searching in the database
	public BankAccount(int accountNumber) {
		super();
		this.accountNumber = accountNumber;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public long getAccountId() {
		return accountId;
	}

	public Transfer payTo(final BigDecimal amount, final BankAccount beneficiary, String description) throws OverdraftException{
		logger.debug("start of payTo");
		BigDecimal margin = getBalance().add(getCreditLimit());
		if(amount.compareTo(margin) == 1){
			BigDecimal overdraft = margin.subtract(amount);
			//German locale to print ',' as decimal separator and '.' as thousands separator
			throw new OverdraftException(String.format(Locale.GERMAN,OVERDRAFT_MSG, amount, accountId, accountNumber, balance, creditLimit, overdraft));
		}
		this.setBalance(this.getBalance().subtract(amount));
		beneficiary.setBalance(beneficiary.getBalance().add(amount));
		final Transfer result = new Transfer(this, beneficiary, new Date(), description, amount);
		logger.debug("payTo ends successfully");
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (accountNumber != other.accountNumber)
			return false;
		return true;
	}
	
}
