package nl.vea.javacourse.bankpayments;

/**
 * Is thrown when an account is removed from the database before the amount on the balance is exactly 0.00
 * @author Willem
 *
 */
public class BalanceNotClearedException extends Exception {

	private static final String DEFAULT_MESSAGE = "This operation is only valid when the balance is 0.00";
	private static final long serialVersionUID = -8217507702530526163L;

	public BalanceNotClearedException() {
		this(DEFAULT_MESSAGE);
	}

	public BalanceNotClearedException(String message) {
		super(message);
	}

	public BalanceNotClearedException(Throwable cause) {
		super(cause);
	}

	public BalanceNotClearedException(String message, Throwable cause) {
		super(message, cause);
	}

	public BalanceNotClearedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
