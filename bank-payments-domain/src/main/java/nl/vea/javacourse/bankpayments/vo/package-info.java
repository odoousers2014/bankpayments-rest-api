/**
 * @author Willem
 * The value objects in this package are Deprecated and shall be replaced with 
 * Jackson MixIn technology in the dependent bank-payments-rest-jersey-xml project.
 * As the bank-payments-rest-jersey-wai probably won't be developed any further,
 * the Value Objects in this package will remain present for now.
 */
@java.lang.Deprecated
package nl.vea.javacourse.bankpayments.vo;