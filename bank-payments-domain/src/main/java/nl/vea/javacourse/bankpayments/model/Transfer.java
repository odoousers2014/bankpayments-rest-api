package nl.vea.javacourse.bankpayments.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;


@Entity
@Table(name="TRANSFERS")
public class Transfer extends JsonEntity implements Serializable{
	
	private static final long serialVersionUID = -7833032689377769954L;

	@Id
	@SequenceGenerator(name = "TRANSFERS_TRANSFER_ID_GENERATOR", sequenceName = "TRANSFERS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRANSFERS_TRANSFER_ID_GENERATOR")
	@Column(name = "TRANSFER_ID", insertable = false, updatable = false)
	private long transferId;
	
	@ManyToOne
	@JoinColumn(name = "DONOR_ACCOUNT_ID", updatable = false)
	private BankAccount donorAccount;
	
	@ManyToOne
	@JoinColumn(name = "BENEFICIARY_ACCOUNT_ID", updatable = false)
	private BankAccount beneficiaryAccount;
	
	@Column(name = "TRANSFER_TIMESTAMP", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transferTimestamp;
	
	@Column(name = "DESCRIPTION", length=250, updatable = false)
	private String description;
	
	@Min(value=0, message="The amount must be more than € 0.00.")
	@Column(name= "AMOUNT", updatable = false)
	private BigDecimal amount;
	
	public Transfer() {
		
	}

	public Transfer(BankAccount donorAccount, BankAccount beneficiaryAccount,
			Date transferTimestamp, String description, BigDecimal amount) {
		super();
		this.donorAccount = donorAccount;
		this.beneficiaryAccount = beneficiaryAccount;
		this.transferTimestamp = transferTimestamp;
		this.description = description;
		this.amount = amount;
	}
	
	public Transfer(BankAccount donorAccount, BankAccount beneficiaryAccount,
			String description, BigDecimal amount) {
		this(donorAccount, beneficiaryAccount, new Date(), description, amount);
	}

	public long getTransferId() {
		return transferId;
	}

	public BankAccount getDonorAccount() {
		return donorAccount;
	}

	public BankAccount getBeneficiaryAccount() {
		return beneficiaryAccount;
	}

	public Date getTransferTimestamp() {
		return transferTimestamp;
	}

	public String getDescription() {
		return description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

}
