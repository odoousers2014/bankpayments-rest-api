package nl.vea.javacourse.bankpayments.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMERS")
@NamedEntityGraphs(@NamedEntityGraph(name = "Customer.accounts", attributeNodes = @NamedAttributeNode("accounts")))
public class Customer  extends JsonEntity implements Serializable {

	private static final long serialVersionUID = 3802231061197339011L;

	@Id
	@SequenceGenerator(name = "CUSTOMERS_CUSTOMER_ID_GENERATOR", sequenceName = "CUSTOMERS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOMERS_CUSTOMER_ID_GENERATOR")
	@Column(name = "CUSTOMER_ID", insertable = false, updatable = false)
	private long customerId;

	@Column(name = "FIRST_NAME", length = 35)
	private String firstName;
	@Column(name = "last_NAME", length = 35)
	private String lastName;

	@Column(name = "ACCOUNT_USER_NAME", length = 35)
	private String accountUserName;

	/**
	 * "Translates in dutch as 'tussenvoegsel', part that is inserted before the last name in dutch names like 'van', 'de' or 'van den'"
	 */
	@Column(name = "NAME_INSERTION", length = 10)
	private String nameInsertion;

	// BankAccount is the owning side
	// The default is lazy fetching
	@OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
	private List<BankAccount> accounts;

	// TODO extend with address etc.

	public Customer() {
		super();
	}

	public Customer(long customerId, String accountUserName, String firstName,
			String lastName, String nameInsertion) {
		super();
		this.customerId = customerId;
		this.accountUserName = accountUserName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nameInsertion = nameInsertion;
	}

	public String getAccountUserName() {
		return accountUserName;
	}

	public void setAccountUserName(String accountUserName) {
		this.accountUserName = accountUserName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getNameInsertion() {
		return nameInsertion;
	}

	public void setNameInsertion(final String nameInsertion) {
		this.nameInsertion = nameInsertion;
	}

	public List<BankAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(final List<BankAccount> accounts) {
		this.accounts = accounts;
	}

	public long getCustomerId() {
		return customerId;
	}

	/**
	 * updates the state with that of the argument, excluding account data
	 * 
	 * @param customer
	 */
	public Customer updateState(final Customer customer) {
		this.customerId = customer.customerId;
		this.firstName = customer.firstName;
		this.lastName = customer.lastName;
		this.nameInsertion = customer.nameInsertion;
		return this;
	}

	// TODO consider whether this method is save
	// TODO reread about proper scaffolding methods
	// see also
	// http://www.javaworld.com/article/2077819/java-se/understanding-jpa-part-2-relationships-the-jpa-way.html
	// public void addAccount(final BankAccount account){
	// if(getAccounts() == null){
	// setAccounts(new ArrayList<BankAccount>());
	// }
	//
	// //this could potentially still lead to a
	// org.hibernate.LazyInitializationException
	// //make the scaffolding part of the owning side of the relation ship (the
	// side with the foreign key in its associated table)
	// getAccounts().add(account);
	// account.setCustomer(this);
	// }
}
