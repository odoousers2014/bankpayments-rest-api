package nl.vea.javacourse.bankpayments.config;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiLocatorDelegate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ch.qos.logback.classic.Level;


//import com.jolbox.bonecp.BoneCPDataSource;

/**
 * An application context Java configuration class. The usage of Java
 * configuration requires Spring Framework 3.0 or higher with following
 * exceptions:
 * <ul>
 * <li>@EnableWebMvc annotation requires Spring Framework 3.1</li>
 * </ul>
 * 
 * @author Willem van Es
 */
@Configuration
@EnableJpaRepositories(basePackages = {"nl.vea.javacourse.bankpayments.repositories"})
@ComponentScan(basePackages = { "nl.vea.javacourse.bankpayments" })
@EnableTransactionManagement
public class SpringDataConfig {

	private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	private static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	private static final String DOMAIN_PACKAGE = "nl.vea.javacourse.bankpayments.model";

	private static final Logger LOG = LoggerFactory.getLogger(SpringDataConfig.class);
	public static void setLoggingLevel(final Level level) {
		((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(level);
	}

	@Bean
	public DataSource dataSource() {
		setLoggingLevel(Level.DEBUG);
		LOG.info("Preparing to lookup datasource 'jdbc/bank-payments-ds' version 2");
		DataSource dataSource = null;		
		LOG.info("Is default JNDI environment available: {}",
				JndiLocatorDelegate.isDefaultJndiEnvironmentAvailable());
		final JndiLocatorDelegate jndi = JndiLocatorDelegate.createDefaultResourceRefLocator();
		try {
			//The datasource for Tomcat 8.0.x is specified by the entry /Context/Resource[@name="jdbc/bank-payments-ds"]
			//inside the file located at /jersey-spring-data-jpa-2.1-poc/src/main/webapp/META-INF/context.xml
			dataSource = jndi.lookup("jdbc/bank-payments-ds", DataSource.class);
		} catch (NamingException e) {
			LOG.error("NamingException for jdbc/bank-payments-ds", e);
		}
		return dataSource;
	}

	@Bean
	public JpaTransactionManager transactionManager()
			throws ClassNotFoundException {
		LOG.info("Creating the JpaTransactionManager");
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory()
				.getObject());
		return transactionManager;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory()
			throws ClassNotFoundException {
		
		LOG.info("Creating the LocalContainerEntityManagerFactoryBean version 3");
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		
		entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
		
		entityManagerFactoryBean.setPackagesToScan(DOMAIN_PACKAGE);
		LOG.info("setPackagesToScan has been called");
		
		LOG.info("Preparing to lookup and set the datasource..");
		DataSource ds = dataSource();
		entityManagerFactoryBean.setDataSource(ds);
		LOG.info("datasource has been set");
		
		entityManagerFactoryBean.setJpaProperties(additionalJpaProperties());
		LOG.info("setJpaProperties has been called");
		entityManagerFactoryBean.afterPropertiesSet();
		
		return entityManagerFactoryBean;
	}

	protected JpaVendorAdapter jpaVendorAdapter() {
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setDatabase(Database.ORACLE);
		
		//show sql logging when trace enabled
		vendorAdapter.setShowSql(LOG.isTraceEnabled());
		return vendorAdapter;
	}

	/**
	 * <p>
	 * Adds additional - specific - Hibernate JPA properties.
	 * </p>
	 * 
	 * @return additional properties
	 */
	protected Properties additionalJpaProperties() {
		final Properties properties = new Properties();
		//TODO change to JPA standard properties
		properties.setProperty(PROPERTY_NAME_HIBERNATE_DIALECT,
				"org.hibernate.dialect.Oracle10gDialect");
		properties.setProperty(PROPERTY_NAME_HIBERNATE_FORMAT_SQL, "true");
		properties.setProperty(PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY,
				"org.hibernate.cfg.ImprovedNamingStrategy");
		
		return properties;
	}
}
