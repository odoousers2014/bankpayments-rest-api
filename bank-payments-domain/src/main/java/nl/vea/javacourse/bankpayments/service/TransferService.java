package nl.vea.javacourse.bankpayments.service;

import java.util.Date;
import java.util.List;

import nl.vea.javacourse.bankpayments.model.Transfer;

public interface TransferService {
	
	/**
	 * Retrieves the Transfer object for the specified id
	 * @param id corresponds with the primary key
	 * @return
	 */
	Transfer findById(Long id);
	
	/**
	 * Retrieves all the Transfer objects more recent than and including the specified date.
	 * @param date
	 * @return
	 */
	List<Transfer> findAllSince(Date date);
	
	/**
	 * Retrieves all the Transfer objects older than and including the specified date.
	 * @param date
	 * @return
	 */
	List<Transfer> findAllUntil(Date date);
	
	
	/**
	 * Retrieves all the Transfer objects between the specified dates (inclusive).
	 * @param from
	 * @param until
	 * @return
	 */
	List<Transfer> findAllBetween(Date from, Date until);
	
	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number.
	 * @param bankAccount
	 * @return
	 */
	List<Transfer> findAllByAccountNumber(int bankAccount);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number more recent than and including the specified date.
	 * @param bankAccount
	 * @param date
	 * @return
	 */
	List<Transfer> findAllSince(int bankAccount, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number older than and including the specified date.
	 * @param bankAccount
	 * @param date
	 * @return
	 */
	List<Transfer> findAllUntil(int bankAccount, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number between the specified dates (inclusive).
	 * @param bankAccount
	 * @param from
	 * @param until
	 * @return
	 */
	List<Transfer> findAllBetween(final int bankAccount, final Date from, final Date until);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified beneficiary account number more recent than and including the specified date.
	 * @param beneficiary account number 
	 * @param date
	 * @return
	 */
	List<Transfer> findAllSinceForBeneficary(int beneficiary, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified beneficiary account number older than and including the specified date.
	 * @param beneficiary account number 
	 * @param date
	 * @return
	 */
	List<Transfer> findAllUntilForBeneficary(int beneficiary, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified beneficiary account number between the specified dates (inclusive).
	 * @param beneficiary account number 
	 * @param from
	 * @param until
	 * @return
	 */
	List<Transfer> findAllBetweenForBeneficary(int beneficiary, Date from, Date until);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified donor account number more recent than and including the specified date.
	 * @param donor account number 
	 * @param date
	 * @return
	 */
	List<Transfer> findAllSinceForDonor(int donor, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified donor older account number than and including the specified date.
	 * @param donor account number 
	 * @param date
	 * @return
	 */
	List<Transfer> findAllUntilForDonor(int donor, Date date);
	
	
	/**
	 * Retrieves all the Transfer objects for the specified donor account number between the specified dates (inclusive).
	 * @param donor account number 
	 * @param from
	 * @param until
	 * @return
	 */
	List<Transfer> findAllBetweenForDonor(int donor, Date from, Date until);
}
