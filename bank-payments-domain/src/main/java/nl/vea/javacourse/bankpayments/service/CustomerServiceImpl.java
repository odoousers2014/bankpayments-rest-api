package nl.vea.javacourse.bankpayments.service;

import java.util.List;

import javax.annotation.Resource;

import nl.vea.javacourse.bankpayments.CustomerNotFoundException;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.repositories.CustomerRepository;
import nl.vea.javacourse.bankpayments.vo.CustomerVO;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Resource
	CustomerRepository customerRepository;
	
	public CustomerServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@Transactional(readOnly = true)
	public Customer findById(Long id) {
		return customerRepository.findOneWithAccounts(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Customer> findByName(String firstName, String insertion,
			String lastName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	@Transactional
	public Customer createNewCustomer(CustomerVO customer) {
		return createNewCustomer(customer.translateToCustomerEntity());
	}
	
	@Override
	@Transactional
	public Customer createNewCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	@Transactional(rollbackFor = CustomerNotFoundException.class)
	public Customer updateCustomer(Customer customer)
			throws CustomerNotFoundException {
		if (!customerRepository.exists(customer.getCustomerId())) {
			throw new CustomerNotFoundException(String.format(CustomerNotFoundException.MSG_TEMPLATE, customer.getCustomerId()));
		}
		return customerRepository.save(customer);
	}

	

	@Override
	@Transactional(rollbackFor = CustomerNotFoundException.class)
	public Customer updateCustomer(CustomerVO customerVO)
			throws CustomerNotFoundException {
		if (!customerRepository.exists(customerVO.getCustomerId())) {
			throw new CustomerNotFoundException(String.format(CustomerNotFoundException.MSG_TEMPLATE, customerVO.getCustomerId()));
		}
		Customer customer = customerRepository.findOne(customerVO.getCustomerId());
		return updateCustomer(customer.updateState(customerVO.translateToCustomerEntity()));
	}

}
