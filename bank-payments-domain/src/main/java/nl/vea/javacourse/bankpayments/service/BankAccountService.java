package nl.vea.javacourse.bankpayments.service;

import java.math.BigDecimal;
import java.util.List;

import nl.vea.javacourse.bankpayments.BalanceNotClearedException;
import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.vo.BankAccountVO;


public interface BankAccountService {
	BankAccount findByAccountNumber(int accountNumber);
	
	List<BankAccount> findAllForCustomer(Customer customer);
	
	List<BankAccount> findAllForCustomerId(Long customerId);

	BankAccount update(BankAccount bankAccount)
			throws BankAccountNotFoundException;
	
	BankAccount createNewAccountFor(int accountNumber, Customer customer);
	
	BankAccount createNewAccountFor(final Long customerId);
	
	BankAccount createNewAccountFor(final Long customerId, final BankAccountVO bankAccountVO);

	//Should we expose this method?
	BankAccount createOrUpdate(BankAccount bankAccount);
	
	void delete(BankAccount bankAccount) throws BalanceNotClearedException;

	BankAccount findById(Long id);

	List<BankAccount> findAll();
	
	/**
	 * Transfers the amount from the donor account to the receiver account
	 * @param amount to be transferred
	 * @param donor account the money is transferred from
	 * @param beneficiary account the money is transferred to
	 * @param description to document specifics
	 * @return persisted record of the payment
	 * @throws OverdraftException when the sum of the donor account's balance and creditLimit is not insufficient to donate the amount
	 * @throws BankAccountNotFoundException if either of the two accounts is not yet known in the database
	 */
	Transfer transfer(BigDecimal amount, BankAccount donor, BankAccount beneficiary, String description) throws BankAccountNotFoundException, OverdraftException;
}
