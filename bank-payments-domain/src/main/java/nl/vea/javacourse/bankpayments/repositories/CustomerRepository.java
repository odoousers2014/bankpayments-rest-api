package nl.vea.javacourse.bankpayments.repositories;

import javax.persistence.NamedEntityGraph;

import nl.vea.javacourse.bankpayments.model.Customer;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	/**
	 * Eager fetching version of {@link #findOne(Long)}} by using the the new JPA 2.1
	 * {@link EntityGraph} annotation refering to the {@link NamedEntityGraph} declared on the {@link Customer} entity.
	 * @see <a href="https://jira.spring.io/browse/DATAJPA-466">https://jira.spring.io/browse/DATAJPA-466</a>
	 * @param customerId
	 * @return
	 */
	@EntityGraph(value = "Customer.accounts", type = EntityGraphType.LOAD)
	@Query(value="SELECT cr FROM Customer cr WHERE cr.customerId = :customerId")
	Customer findOneWithAccounts(@Param("customerId") Long customerId);

}
