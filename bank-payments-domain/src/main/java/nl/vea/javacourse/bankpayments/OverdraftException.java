/**
 * 
 */
package nl.vea.javacourse.bankpayments;

import java.math.BigDecimal;

/**
 * @author a141185
 * 
 */
public class OverdraftException extends Exception {
	private static final String DEFAULT_MESSAGE = "This transaction would extend beyond the allowed limit of credit";
	
	private static final long serialVersionUID = 6322363349672788789L;
	
	private BigDecimal overdraft;
	
	private BigDecimal maximumAllowed;

	/**
	 * 
	 */
	public OverdraftException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 */
	public OverdraftException(String message) {
		super(message);
	}
	
	public OverdraftException(String message, BigDecimal overdraft, BigDecimal maximumAllowed) {
		super(message);
		this.overdraft = overdraft;
		this.maximumAllowed = maximumAllowed;
	}

	/**
	 * @param cause
	 */
	public OverdraftException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public OverdraftException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param arg2
	 * @param arg3
	 */
	public OverdraftException(String message, Throwable cause, boolean arg2,
			boolean arg3) {
		super(message, cause, arg2, arg3);
	}

	public BigDecimal getOverdraft() {
		return overdraft;
	}

	public void setOverdraft(BigDecimal overdraft) {
		this.overdraft = overdraft;
	}

	public BigDecimal getMaximumAllowed() {
		return maximumAllowed;
	}

	public void setMaximumAllowed(BigDecimal maximumAllowed) {
		this.maximumAllowed = maximumAllowed;
	}
	
}
