package nl.vea.javacourse.bankpayments.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.repositories.TransferRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransferServiceImpl implements TransferService {

	@Resource
	TransferRepository transferRepository;
	
	public TransferServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Transactional(readOnly = true)
	@Override
	public Transfer findById(Long id) {
		// TODO Auto-generated method stub
		return transferRepository.findOne(id);
	}

	@Override
	public List<Transfer> findAllSince(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllUntil(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllBetween(Date from, Date until) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public List<Transfer> findAllByAccountNumber(int bankAccount) {
		return transferRepository.findAllByAccountNumber(bankAccount);
	}

	@Override
	public List<Transfer> findAllSince(int bankAccount, Date date) {
		return transferRepository.findAllSince(bankAccount, date);
	}

	@Override
	public List<Transfer> findAllUntil(int bankAccount, Date date) {
		// TODO Auto-generated method stub
		return transferRepository.findAllUntil(bankAccount, date);
	}

	@Override
	public List<Transfer> findAllBetween(final int bankAccount, final Date from, final Date until) {
		return transferRepository.findAllBetween(bankAccount, from, until);
	}

	@Override
	public List<Transfer> findAllSinceForBeneficary(int beneficiary, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllUntilForBeneficary(int beneficiary, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllBetweenForBeneficary(int beneficiary,
			Date from, Date until) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllSinceForDonor(int donor, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllUntilForDonor(int donor, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transfer> findAllBetweenForDonor(int donor, Date from,
			Date until) {
		// TODO Auto-generated method stub
		return null;
	}

}
