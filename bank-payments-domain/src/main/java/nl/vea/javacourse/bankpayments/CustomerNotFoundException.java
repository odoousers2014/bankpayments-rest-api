package nl.vea.javacourse.bankpayments;

public class CustomerNotFoundException extends Exception {

	public static final String MSG_TEMPLATE = "The Customer with id %s was not found in the database.";
	
	private static final long serialVersionUID = 482107145813720372L;

	public CustomerNotFoundException() {
	}

	public CustomerNotFoundException(String message) {
		super(message);
	}

	public CustomerNotFoundException(Throwable cause) {
		super(cause);
	}

	public CustomerNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomerNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
