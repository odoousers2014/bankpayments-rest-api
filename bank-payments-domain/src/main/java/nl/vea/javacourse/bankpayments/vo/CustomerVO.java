package nl.vea.javacourse.bankpayments.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nl.vea.javacourse.bankpayments.model.Customer;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;


public class CustomerVO implements Serializable {

	private static final long serialVersionUID = -3855435284894916127L;

	private long customerId;
	
	private String accountUserName;

	private String firstName;

	private String lastName;

	/**
	 * "Translates in dutch as 'tussenvoegsel', part that is inserted before the last name in dutch names like 'van', 'de' or 'van den'"
	 */
	private String nameInsertion;

	private List<Long> accounts;

	public CustomerVO() {
	}

	@SuppressWarnings("unchecked")
	public CustomerVO(final Customer customer) {
		this.customerId = customer.getCustomerId();
		this.accountUserName = customer.getAccountUserName();
		this.firstName = customer.getFirstName();
		this.nameInsertion = customer.getNameInsertion();
		this.lastName = customer.getLastName();
		this.accounts = (List<Long>) CollectionUtils
				.collect(customer.getAccounts(),
						new BeanToPropertyValueTransformer("accountId"));
	}

	
	public String getAccountUserName() {
		return accountUserName;
	}

	public void setAccountUserName(String accountUserName) {
		this.accountUserName = accountUserName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getNameInsertion() {
		return nameInsertion;
	}

	public void setNameInsertion(final String nameInsertion) {
		this.nameInsertion = nameInsertion;
	}

	public List<Long> getAccounts() {
		return accounts;
	}

	public void setAccounts(final List<Long> accounts) {
		this.accounts = accounts;
	}

	public long getCustomerId() {
		return customerId;
	}
	
	/**
	 * Creates a {@link Customer} entity based on the state of this object. AccountInfo, however, is skipped.
	 * @return
	 */
	public Customer translateToCustomerEntity(){
		Customer customer = new Customer(getCustomerId(), getAccountUserName(), getFirstName(), getLastName(), getNameInsertion());
		return customer;
	}
	
	public static List<CustomerVO> getVOfromEntities(
			final List<Customer> customers) {
		final List<CustomerVO> result = new ArrayList<>();
		for (final Customer customer : customers) {
			result.add(new CustomerVO(customer));
		}
		return result;
	}

	@Override
	public String toString() {
		return "CustomerVO [customerId=" + customerId + ", accountUserName="
				+ accountUserName + ", firstName=" + firstName + ", lastName="
				+ lastName + ", nameInsertion=" + nameInsertion + ", accounts="
				+ accounts + "]";
	}
	
}
