package nl.vea.javacourse.bankpayments.service;

import java.util.List;

import nl.vea.javacourse.bankpayments.CustomerNotFoundException;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.vo.CustomerVO;

public interface CustomerService {
	Customer findById(Long id);

	List<Customer> findByName(String firstName, String insertion, String lastName);
	
	List<Customer> findAll();
	
	Customer createNewCustomer(Customer customer);
	
	Customer createNewCustomer(CustomerVO customer);
	
	Customer updateCustomer(Customer customer) throws CustomerNotFoundException;
	
	Customer updateCustomer(CustomerVO customer) throws CustomerNotFoundException;
}
