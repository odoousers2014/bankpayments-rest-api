package nl.vea.javacourse.bankpayments.repositories;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import nl.vea.javacourse.bankpayments.model.Transfer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

public interface TransferRepository extends JpaRepository<Transfer, Long> {

	public static final String QUERY_FIND_ALL_BY_ACCOUNT_NUMBER = "SELECT tr FROM Transfer tr"
			+ " inner join tr.donorAccount don"
			+ " inner join tr.beneficiaryAccount ben"
			+ " WHERE (don.accountNumber = :accountNumber or ben.accountNumber = :accountNumber)";
	
	public static final String ORDER_BY_TRANSFER_TIMESTAMP_DESC = " ORDER BY tr.transferTimestamp DESC";

	public static final String QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_SINCE = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER
			+ " AND tr.transferTimestamp >= :since"
			+ ORDER_BY_TRANSFER_TIMESTAMP_DESC;

	public static final String QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_UNTIL = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER
			+ " AND tr.transferTimestamp <= :until"
			+ ORDER_BY_TRANSFER_TIMESTAMP_DESC;

	public static final String QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_BETWEEN = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER
			+ " AND tr.transferTimestamp BETWEEN :from AND :until"
			+ ORDER_BY_TRANSFER_TIMESTAMP_DESC;

	// /**
	// * Retrieves all the Transfer objects more recent than and including the
	// specified date.
	// * @param date
	// * @return
	// */
	// List<Transfer> findAllSince(@Temporal(TemporalType.TIMESTAMP) Date date);
	//
	// /**
	// * Retrieves all the Transfer objects older than and including the
	// specified date.
	// * @param date
	// * @return
	// */
	// List<Transfer> findAllOlderThan(@Temporal(TemporalType.TIMESTAMP) Date
	// date);
	//
	//
	// /**
	// * Retrieves all the Transfer objects between the specified dates
	// (inclusive).
	// * @param from
	// * @param until
	// * @return
	// */
	// List<Transfer> findAllBetween(@Temporal(TemporalType.TIMESTAMP) Date
	// from, @Temporal(TemporalType.TIMESTAMP) Date until);
	
	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number
	 * more recent than and including the specified date.
	 * 
	 * @param bankAccount
	 * @param date
	 * @return
	 */
	@Query(value = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER + ORDER_BY_TRANSFER_TIMESTAMP_DESC)
	List<Transfer> findAllByAccountNumber(@Param("accountNumber") final int bankAccount);

	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number
	 * more recent than and including the specified date.
	 * 
	 * @param bankAccount
	 * @param date
	 * @return
	 */
	@Query(value = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_SINCE)
	List<Transfer> findAllSince(@Param("accountNumber") final int bankAccount,
			@Param("since") @Temporal(TemporalType.TIMESTAMP) final Date date);

	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number
	 * older than and including the specified date.
	 * 
	 * @param bankAccount
	 * @param date
	 * @return
	 */
	@Query(value = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_UNTIL)
	List<Transfer> findAllUntil(
			@Param("accountNumber") final int bankAccount,
			@Param("until") @Temporal(TemporalType.TIMESTAMP) Date date);

	/**
	 * Retrieves all the Transfer objects for the specified bankAccount number
	 * between the specified dates (inclusive).
	 * 
	 * @param bankAccount
	 * @param from
	 * @param until
	 * @return
	 */

	@Query(value = QUERY_FIND_ALL_BY_ACCOUNT_NUMBER_BETWEEN)
	List<Transfer> findAllBetween(
			@Param("accountNumber") final int bankAccount,
			@Param("from") @Temporal(TemporalType.TIMESTAMP) final Date from,
			@Param("until") @Temporal(TemporalType.TIMESTAMP) final Date until);

	// /**
	// * Retrieves all the Transfer objects for the specified beneficiary
	// account number more recent than and including the specified date.
	// * @param beneficiary account number
	// * @param date
	// * @return
	// */
	// List<Transfer> findAllSinceForBeneficary(int beneficiary,
	// @Temporal(TemporalType.TIMESTAMP) Date date);
	//
	//
	// /**
	// * Retrieves all the Transfer objects for the specified beneficiary
	// account number older than and including the specified date.
	// * @param beneficiary account number
	// * @param date
	// * @return
	// */
	// List<Transfer> findOlderThanForBeneficary(int beneficiary,
	// @Temporal(TemporalType.TIMESTAMP) Date date);
	//
	//
	// /**
	// * Retrieves all the Transfer objects for the specified beneficiary
	// account number between the specified dates (inclusive).
	// * @param beneficiary account number
	// * @param from
	// * @param until
	// * @return
	// */
	// List<Transfer> findAllBetweenForBeneficary(int beneficiary,
	// @Temporal(TemporalType.TIMESTAMP) Date from,
	// @Temporal(TemporalType.TIMESTAMP) Date until);
	//
	//
	// /**
	// * Retrieves all the Transfer objects for the specified donor account
	// number more recent than and including the specified date.
	// * @param donor account number
	// * @param date
	// * @return
	// */
	// List<Transfer> findAllSinceForDonor(int donor,
	// @Temporal(TemporalType.TIMESTAMP) Date date);
	//
	//
	// /**
	// * Retrieves all the Transfer objects for the specified donor older
	// account number than and including the specified date.
	// * @param donor account number
	// * @param date
	// * @return
	// */
	// List<Transfer> findOlderThanForDonor(int donor,
	// @Temporal(TemporalType.TIMESTAMP) Date date);
	//
	//
	// /**
	// * Retrieves all the Transfer objects for the specified donor account
	// number between the specified dates (inclusive).
	// * @param donor account number
	// * @param from
	// * @param until
	// * @return
	// */
	// List<Transfer> findAllBetweenForDonor(int donor,
	// @Temporal(TemporalType.TIMESTAMP) Date from,
	// @Temporal(TemporalType.TIMESTAMP) Date until);
}
