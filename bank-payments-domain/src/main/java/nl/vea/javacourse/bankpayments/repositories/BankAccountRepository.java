package nl.vea.javacourse.bankpayments.repositories;

import java.util.List;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

//Annotation suggested in https://blog.jetbrains.com/idea/2011/11/enjoy-spring-data-jpa-in-intellij-11/
//but is superfluous: Repositories injected with @Resource are now recognized by IntelliJ IDEA 2016.1.3
//@RepositoryDefinition(domainClass = BankAccount.class, idClass = Long.class)
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
	public static final String QUERY_FIND_BY_ACCOUNT_NUMBER = "SELECT ba FROM BankAccount ba WHERE ba.accountNumber = :accountNumber";
	public static final String QUERY_FIND_ALL_BY_CUSTOMER = "SELECT ba FROM BankAccount ba WHERE ba.customer = :customer";
	public static final String QUERY_FIND_ALL_BY_CUSTOMER_ID = "SELECT ba FROM BankAccount ba INNER JOIN ba.customer cr WHERE cr.customerId = :customerId";

	/**
	 * Finds an {@link BankAccount} identified by the given <code>accountNumber</code>.
	 * @param accountNumber identifies the {@link BankAccount} to retrieve
	 * @return found {@link BankAccount} or <code>null</code> if not found.
	 */
	@Query(value = QUERY_FIND_BY_ACCOUNT_NUMBER)
	BankAccount findByAccountNumber(@Param("accountNumber") final int accountNumber);
	
	/**
	 * Finds a list of  {@link BankAccount} associated with the given <code>customer</code>.
	 * @param customer associated with the list of {@link BankAccount} to retrieve
	 * @return found {@link List<BankAccount>} or <code>null</code> if none found.
	 */
	@Query(value = QUERY_FIND_ALL_BY_CUSTOMER)
	List<BankAccount> findAllForCustomer(@Param("customer") final Customer customer);
	
	/**
	 * Finds a list of  {@link BankAccount} associated with the given <code>customer</code>.
	 * @param customer associated with the list of {@link BankAccount} to retrieve
	 * @return found {@link List<BankAccount>} or <code>null</code> if none found.
	 */
	@Query(value = QUERY_FIND_ALL_BY_CUSTOMER_ID)
	List<BankAccount> findAllForCustomerId(@Param("customerId") final Long customerId);
}
