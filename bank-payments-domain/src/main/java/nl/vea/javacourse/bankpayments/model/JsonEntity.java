package nl.vea.javacourse.bankpayments.model;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * An abstract superclass to facilitate polymorphic deserialization from JSON
 * with the Jackson library
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = BankAccount.class, name = "bankAccount"),
		@Type(value = Customer.class, name = "customer"),
		@Type(value = Transfer.class, name = "transfer")})
public abstract class JsonEntity {
	private List<Link> _links = new ArrayList<>();

	public List<Link> get_links() {
		return _links;
	}

	public void set_links(List<Link> _links) {
		this._links = _links;
	}
	
	public void addLink(Link link){
		this._links.add(link);
	}
}
