package nl.vea.javacourse.bankpayments.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Min;

import nl.vea.javacourse.bankpayments.model.Transfer;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransferVO implements Serializable{

	private static final long serialVersionUID = 2256250685135414786L;

	private long transferId;
	

	private Integer donorAccountNumber;
	

	private Integer beneficiaryAccountNumber;
	
	
	/**
	 * Using {@link JsonFormat} to obtain the Atom (ISO 8601) pattern in JSON representations
	 * {@linkplain http://www.fileformat.info/tip/java/simpledateformat.htm}
	 * {@linkplain http://www.baeldung.com/jackson-serialize-dates}
	 */
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssz", timezone="CET")
	private Date transferTimestamp;
	

	private String description;
	
	@Min(value=0, message="The amount must be more than € 0.00.")
	private BigDecimal amount;
	
	public TransferVO() {
		super();
	}
	
	public TransferVO(Integer donorAccountNr, Integer BeneficiaryAccountNr, BigDecimal amount, String description) {
		super();
		this.donorAccountNumber = donorAccountNr;
		this.beneficiaryAccountNumber = BeneficiaryAccountNr;
		this.amount = amount;
		this.description = description;
	}

	public TransferVO(Transfer transfer) {
		super();
		this.transferId = transfer.getTransferId();
		this.donorAccountNumber = transfer.getDonorAccount().getAccountNumber();
		this.beneficiaryAccountNumber = transfer.getBeneficiaryAccount().getAccountNumber();
		this.transferTimestamp = transfer.getTransferTimestamp();
		this.description = transfer.getDescription();
		this.amount = transfer.getAmount();
	}

	public long getTransferId() {
		return transferId;
	}

	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}

	public Integer getDonorAccountNumber() {
		return donorAccountNumber;
	}

	public void setDonorAccountNumber(Integer donorAccount) {
		this.donorAccountNumber = donorAccount;
	}

	public Integer getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(Integer beneficiaryAccount) {
		this.beneficiaryAccountNumber = beneficiaryAccount;
	}

	public Date getTransferTimestamp() {
		return transferTimestamp;
	}

	public void setTransferTimestamp(Date transferTimestamp) {
		this.transferTimestamp = transferTimestamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	public static List<TransferVO> getVOfromEntities(
			final List<Transfer> transfers) {
		final List<TransferVO> result = new ArrayList<>();
		for (final Transfer transfer : transfers) {
			result.add(new TransferVO(transfer));
		}
		return result;
	}
}
