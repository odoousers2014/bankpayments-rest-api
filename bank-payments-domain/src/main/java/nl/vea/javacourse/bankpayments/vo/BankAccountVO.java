/**
 * 
 */
package nl.vea.javacourse.bankpayments.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import nl.vea.javacourse.bankpayments.model.BankAccount;

/**
 * @author a141185
 *
 */
public class BankAccountVO implements Serializable {

	private static final long serialVersionUID = 3907203752415926112L;

	private long accountId;

	private int accountNumber;

	private BigDecimal balance;

	private BigDecimal creditLimit;

	private CustomerVO customer;

	public BankAccountVO() {
	}

	public BankAccountVO(final BankAccount bankAccount) {
		this.accountId = bankAccount.getAccountId();
		this.accountNumber = bankAccount.getAccountNumber();
		this.balance = bankAccount.getBalance();
		this.creditLimit = bankAccount.getCreditLimit();
		this.customer = new CustomerVO(bankAccount.getCustomer());
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(final BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(final BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(final CustomerVO customer) {
		this.customer = customer;
	}

	public long getAccountId() {
		return accountId;
	}
	
	public static List<BankAccountVO> getVOfromEntities(
			final List<BankAccount> bankAccounts) {
		final List<BankAccountVO> result = new ArrayList<>();
		for (final BankAccount bankAccount : bankAccounts) {
			result.add(new BankAccountVO(bankAccount));
		}
		return result;
	}

	@Override
	public String toString() {
		return "BankAccountVO [accountId=" + accountId + ", accountNumber="
				+ accountNumber + ", balance=" + balance + ", creditLimit="
				+ creditLimit + ", customer=" + customer + "]";
	}
	
	
}
