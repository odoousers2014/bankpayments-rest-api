package nl.vea.javacourse.bankpayments.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import nl.vea.javacourse.bankpayments.BalanceNotClearedException;
import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.config.SpringDataConfig;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.repositories.BankAccountRepository;
import nl.vea.javacourse.bankpayments.repositories.CustomerRepository;
import nl.vea.javacourse.bankpayments.repositories.TransferRepository;
import nl.vea.javacourse.bankpayments.vo.BankAccountVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BankAccountServiceImpl implements BankAccountService {
	private final Logger logger = LoggerFactory.getLogger(BankAccountService.class);

	@Resource
	BankAccountRepository bankAccountRepository;
	
	@Resource
	TransferRepository transferRepository;
	
	@Resource
	CustomerRepository customerRepository;
	
	@Transactional(readOnly = true)
	@Override
	public BankAccount findById(Long id) {
		return bankAccountRepository.findOne(id);
	}

	@Transactional(readOnly = true)
	@Override
	public List<BankAccount> findAll() {
		return bankAccountRepository.findAll();
	}
	
	@Override
	@Transactional
	public BankAccount createOrUpdate(BankAccount bankAccount) {
		return bankAccountRepository.save(bankAccount);
	}
	
	@Override
	@Transactional(rollbackFor = BankAccountNotFoundException.class)
	public BankAccount update(BankAccount bankAccount) throws BankAccountNotFoundException {
		if (!bankAccountRepository.exists(bankAccount.getAccountId())) {
			throw new BankAccountNotFoundException(bankAccount.getAccountId());
		}
		return bankAccountRepository.save(bankAccount);
	}
	
	@Override
	@Transactional(readOnly = true)
	public BankAccount findByAccountNumber(int accountNumber) {
		return bankAccountRepository.findByAccountNumber(accountNumber);
	}

	@Override
	@Transactional(readOnly = true)
	public List<BankAccount> findAllForCustomer(Customer customer) {
		return bankAccountRepository.findAllForCustomer(customer);
	}

	@Override
	@Transactional(readOnly = true)
	public List<BankAccount> findAllForCustomerId(Long customerId) {
		return bankAccountRepository.findAllForCustomerId(customerId);
	}

	@Override
	@Transactional(rollbackFor = {OverdraftException.class, BankAccountNotFoundException.class})
	public Transfer transfer(BigDecimal amount, BankAccount donor,
			BankAccount beneficiary, String description) throws BankAccountNotFoundException, OverdraftException {
		logger.debug("Starting transfer");
		final String msgAccountNotFound = "The account with number %s is unknown";
		if (!bankAccountRepository.exists(donor.getAccountId())) {
			throw new BankAccountNotFoundException(String.format(msgAccountNotFound, donor.getAccountId()));
		}
		logger.debug("Donor account exists");
		if (!bankAccountRepository.exists(beneficiary.getAccountId())) {
			throw new BankAccountNotFoundException(String.format(msgAccountNotFound, beneficiary.getAccountId()));
		}
		final Transfer record = donor.payTo(amount, beneficiary, description);
		bankAccountRepository.save(donor);
		bankAccountRepository.save(beneficiary);
		final Transfer result = transferRepository.save(record);
		logger.debug("Transfer completed successfully");
		return result;
	}

	@Override
	@Transactional
	public BankAccount createNewAccountFor(final int accountNumber, final Customer customer) {
		if (findByAccountNumber(accountNumber) != null) {
			throw new RuntimeException(String.format("accountNumber %s already exists", accountNumber));
		}
		return bankAccountRepository.saveAndFlush(new BankAccount(accountNumber, /*customerRepository.findOneWithAccounts(*/customer/*.getCustomerId())*/));
	}
	
	@Override
	@Transactional
	public BankAccount createNewAccountFor(final Long customerId) {		
		return createNewAccountFor(customerId, null);
	}
	
	

	@Override
	@Transactional
	public BankAccount createNewAccountFor(Long customerId,
			BankAccountVO bankAccountVO) {
		Customer customer = customerRepository.findOne(customerId);
		if(customer == null){
			throw new RuntimeException(String.format("There is no customer for the given id %s.", customerId));
		}
		int accountNumber;
		do{
			accountNumber = 100000000 + new Random().nextInt(900000000);
		}
		while (findByAccountNumber(accountNumber) != null);
		BankAccount bankAccount = new BankAccount(accountNumber, customer);
		if(bankAccountVO != null){
			bankAccount.setBalance(bankAccountVO.getBalance());
			bankAccount.setCreditLimit(bankAccountVO.getCreditLimit());
		}
		return bankAccountRepository.saveAndFlush(bankAccount);
	}

	@Override
	public void delete(BankAccount bankAccount) throws BalanceNotClearedException{
		//using compareTo to disregard scale differences
		if (bankAccount.getBalance().compareTo(BigDecimal.ZERO) != 0) {
			throw new BalanceNotClearedException(String.format("The account with number %s and balance %s cannot "
					+ "be deleted until the balance has been cleared to 0.00", bankAccount.getAccountNumber(), 
					bankAccount.getBalance()));
		}
		bankAccountRepository.delete(bankAccount);		
	}

}
