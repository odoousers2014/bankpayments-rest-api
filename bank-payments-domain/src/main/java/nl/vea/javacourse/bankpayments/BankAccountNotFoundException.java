package nl.vea.javacourse.bankpayments;

public class BankAccountNotFoundException extends Exception {

	private static final long serialVersionUID = -2190818461907126161L;
	
	public static final String MSG_ID_TEMPLATE = "The bank account with id %s was not found in the database.";
	
	public static final String MSG_NR_TEMPLATE = "The bank account with number %s was not found in the database.";

	public BankAccountNotFoundException(Integer accountNumber) {
		super(String.format(MSG_NR_TEMPLATE, accountNumber));
	}
	
	public BankAccountNotFoundException(Long accountId) {
		super(String.format(MSG_ID_TEMPLATE, accountId));
	}

	public BankAccountNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BankAccountNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public BankAccountNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BankAccountNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
