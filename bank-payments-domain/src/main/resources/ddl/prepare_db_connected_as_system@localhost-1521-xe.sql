-- To prevent passwords from expiring
-- run statements in SQL Developer logged in / connected as user 'system'
select * from dba_users;

ALTER USER hr IDENTIFIED BY hr ACCOUNT UNLOCK;
GRANT CONNECT, RESOURCE to hr;
commit;
--changing sid
--http://nenadbulatovic.blogspot.nl/2013/02/change-sid-on-oracle-express-xe-1120.html

--https://communities.netapp.com/community/netapp-blogs/databases/blog/2011/08/10/oracle-11g--password-expires
SELECT profile, resource_name, limit FROM dba_profiles WHERE profile='DEFAULT';
/*
Will yield
PROFILE                        RESOURCE_NAME                    LIMIT                                  
------------------------------ -------------------------------- ----------------------------------------
DEFAULT                        COMPOSITE_LIMIT                  UNLIMITED                                
DEFAULT                        SESSIONS_PER_USER                UNLIMITED                                
DEFAULT                        CPU_PER_SESSION                  UNLIMITED                                
DEFAULT                        CPU_PER_CALL                     UNLIMITED                                
DEFAULT                        LOGICAL_READS_PER_SESSION        UNLIMITED                                
DEFAULT                        LOGICAL_READS_PER_CALL           UNLIMITED                                
DEFAULT                        IDLE_TIME                        UNLIMITED                                
DEFAULT                        CONNECT_TIME                     UNLIMITED                                
DEFAULT                        PRIVATE_SGA                      UNLIMITED                                
DEFAULT                        FAILED_LOGIN_ATTEMPTS            10                                       
DEFAULT                        PASSWORD_LIFE_TIME               180                                      
DEFAULT                        PASSWORD_REUSE_TIME              UNLIMITED                                
DEFAULT                        PASSWORD_REUSE_MAX               UNLIMITED                                
DEFAULT                        PASSWORD_VERIFY_FUNCTION         NULL                                     
DEFAULT                        PASSWORD_LOCK_TIME               1                                        
DEFAULT                        PASSWORD_GRACE_TIME              7                                        

 16 rows selected 
*/
alter profile default limit password_life_time unlimited;
commit;
--again 
SELECT profile, resource_name, limit FROM dba_profiles WHERE profile='DEFAULT';
/*
Now yields:
PROFILE                        RESOURCE_NAME                    LIMIT                                  
------------------------------ -------------------------------- ----------------------------------------
DEFAULT                        COMPOSITE_LIMIT                  UNLIMITED                                
DEFAULT                        SESSIONS_PER_USER                UNLIMITED                                
DEFAULT                        CPU_PER_SESSION                  UNLIMITED                                
DEFAULT                        CPU_PER_CALL                     UNLIMITED                                
DEFAULT                        LOGICAL_READS_PER_SESSION        UNLIMITED                                
DEFAULT                        LOGICAL_READS_PER_CALL           UNLIMITED                                
DEFAULT                        IDLE_TIME                        UNLIMITED                                
DEFAULT                        CONNECT_TIME                     UNLIMITED                                
DEFAULT                        PRIVATE_SGA                      UNLIMITED                                
DEFAULT                        FAILED_LOGIN_ATTEMPTS            10                                       
DEFAULT                        PASSWORD_LIFE_TIME               UNLIMITED                                
DEFAULT                        PASSWORD_REUSE_TIME              UNLIMITED                                
DEFAULT                        PASSWORD_REUSE_MAX               UNLIMITED                                
DEFAULT                        PASSWORD_VERIFY_FUNCTION         NULL                                     
DEFAULT                        PASSWORD_LOCK_TIME               1                                        
DEFAULT                        PASSWORD_GRACE_TIME              7                                        

 16 rows selected 

*/

--Create the schema's bank_payments and bank_payments_test and grant sufficient rights to run ddl scripts.
create user bank_payments identified by bank_payments;
grant connect, resource to bank_payments;
create user bank_payments_test identified by bank_payments_test;
grant connect, resource to bank_payments_test;
--change oracle admin webserver port from 8080 to 8081 to avod conflict with the defaults of the Tomcat webserver/servlet engine
EXEC DBMS_XDB.SETHTTPPORT(8081);
commit;