set feedback on
set heading on
set linesize 500
set pages 52
set echo on
--Beware environment specific log file location
--spool  C:\Users\Willem\application-logs\bank-payments\database\TransfersReport.log
spool  C:\Users\m07f013\application-logs\bank-payments\database\TransfersReport.log

select ts.TRANSFER_ID, 
ts.TRANSFER_TIMESTAMP,
ts.AMOUNT,
adon.ACCOUNT_NUMBER as "donor acc",
(case when cdon.NAME_INSERTION is null then cdon.FIRST_NAME || ' ' || cdon.LAST_NAME else cdon.FIRST_NAME || ' ' || cdon.NAME_INSERTION ||  ' ' || cdon.LAST_NAME end)  as "donor",
aben.ACCOUNT_NUMBER as "benef acc",
 (case when cben.NAME_INSERTION is null then cben.FIRST_NAME || ' ' || cben.LAST_NAME else cben.FIRST_NAME || ' ' || cben.NAME_INSERTION ||  ' ' || cben.LAST_NAME end)  as "beneficiary",
 ts.DESCRIPTION
from transfers ts 
inner join BANK_ACCOUNTS adon on adon.ACCOUNT_ID = ts.DONOR_ACCOUNT_ID
inner join BANK_ACCOUNTS aben on aben.ACCOUNT_ID = ts.BENEFICIARY_ACCOUNT_ID
inner join customers cdon on cdon.CUSTOMER_ID = adon.CUSTOMER_ID
inner join customers cben on cben.CUSTOMER_ID = aben.CUSTOMER_ID
order by ts.TRANSFER_TIMESTAMP desc;

select
act.ACCOUNT_NUMBER as "account",
(case when cust.NAME_INSERTION is null then cust.FIRST_NAME || ' ' || cust.LAST_NAME else cust.FIRST_NAME || ' ' || cust.NAME_INSERTION ||  ' ' || cust.LAST_NAME end)  as "customer",
act.BALANCE as "balance"
from  BANK_ACCOUNTS act
inner join customers cust on cust.CUSTOMER_ID = act.CUSTOMER_ID
order by cust.LAST_NAME asc;

set echo off
spool off