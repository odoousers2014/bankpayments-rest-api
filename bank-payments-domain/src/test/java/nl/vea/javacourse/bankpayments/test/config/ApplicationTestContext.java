package nl.vea.javacourse.bankpayments.test.config;

import java.util.Properties;

import javax.sql.DataSource;

import nl.vea.javacourse.bankpayments.config.SpringDataConfig;

import org.hsqldb.jdbcDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * An application context Java configuration class. The usage of Java configuration requires Spring
 * Framework 3.0 or higher with following exceptions:
 * <ul>
 * <li>@EnableWebMvc annotation requires Spring Framework 3.1</li>
 * </ul>
 * 
 * @author Petri Kainulainen
 */
@Configuration
@EnableJpaRepositories(basePackages = { "nl.vea.javacourse.bankpayments.repositories" })
@ComponentScan(basePackages = { "nl.vea.javacourse.bankpayments" })
@EnableTransactionManagement
public class ApplicationTestContext extends SpringDataConfig {

	private static final String HSQLDB_MEM_URL = "jdbc:hsqldb:mem:bp;sql.syntax_ora=true";

	private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	private static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationTestContext.class);

	/**
	 * <p>
	 * Sets up the {@link DataSource}.
	 * </p>
	 * 
	 * @return the {@link DataSource}.</p>
	 */
	@Bean
	public DataSource dataSource() {
		return new SimpleDriverDataSource(jdbcDriver.driverInstance, HSQLDB_MEM_URL, "sa", "");
	}

	protected JpaVendorAdapter jpaVendorAdapter() {
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setDatabase(Database.HSQL);
		LOG.debug(
				"Hibernate generated sql statements are shown when LOG.isTraceEnabled(), which is currently {}",
				LOG.isTraceEnabled());
		vendorAdapter.setShowSql(LOG.isTraceEnabled());
		return vendorAdapter;
	}

	/**
	 * <p>
	 * Adds additional - specific - Hibernate JPA properties.
	 * </p>
	 * 
	 * @return additional properties
	 */
	protected Properties additionalJpaProperties() {
		final Properties properties = new Properties();
		// TODO change to JPA standard properties
		properties.setProperty(PROPERTY_NAME_HIBERNATE_DIALECT,
				"org.hibernate.dialect.Oracle10gDialect");
		properties.setProperty(PROPERTY_NAME_HIBERNATE_FORMAT_SQL, "true");
		properties.setProperty(PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY,
				"org.hibernate.cfg.ImprovedNamingStrategy");

		return properties;
	}
}
