package nl.vea.javacourse.bankpayments.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;

import nl.vea.javacourse.bankpayments.BalanceNotClearedException;
import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.repositories.TransferRepository;
import nl.vea.javacourse.bankpayments.service.TransferService;
import nl.vea.javacourse.bankpayments.test.config.ApplicationTestContext;
import nl.vea.javacourse.bankpayments.test.util.PersistenceTestUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestContext.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class TransferServiceTest {

	@Resource
	TransferRepository transferRepository;
	
	@Inject
	private TransferService transferService;

	@Autowired
	private DataSource datasource;

	@Before
	public void setUp() throws Exception {
		assertNotNull(datasource);
		PersistenceTestUtils
				.executeSQL(
						datasource.getConnection(),
						Thread.currentThread()
								.getContextClassLoader()
								.getResourceAsStream(
										"ddl/Bank_Payments_DDL_HSQLDB.sql"));
		PersistenceTestUtils.executeSQL(
				datasource.getConnection(),
				Thread.currentThread()
						.getContextClassLoader()
						.getResourceAsStream(
								"ddl/Bank_Payments_Inserts_HSQLDB.sql"));
	}

	@Test
	public void testCRUD() throws BankAccountNotFoundException, BalanceNotClearedException {
		assertNotNull(transferRepository);
		assertNotNull(transferService);
		assertEquals(5L, transferRepository.count());
		final Calendar from = Calendar.getInstance();
		from.set(2012, 0, 1, 12, 42, 59);
		final Calendar until = Calendar.getInstance();
//		until.set(2015, 0, 3, 01, 30, 59);
		final List<Transfer> transfers = transferService.findAllBetween(453850855, from.getTime(), until.getTime());
		
//		List<Transfer> transfers = transferService.findAllByAccountNumber(453850855);
		assertEquals(3, transfers.size());
		assertEquals(BigDecimal.valueOf(39.99), transfers.get(0).getAmount());
		assertEquals(BigDecimal.valueOf(23.75), transfers.get(1).getAmount());
		assertEquals(653377991, transfers.get(0).getDonorAccount().getAccountNumber());
		assertEquals(114956154, transfers.get(1).getBeneficiaryAccount().getAccountNumber());
		
		until.set(2015, 0, 3, 01, 30, 59);
		
		final List<Transfer> transfersAllSince = transferService.findAllSince(453850855, until.getTime());
		assertEquals(1, transfersAllSince.size());
		assertEquals(BigDecimal.valueOf(39.99), transfersAllSince.get(0).getAmount());
		
		final List<Transfer> transfersAllUntil = transferService.findAllUntil(453850855, until.getTime());
		assertEquals(2, transfersAllUntil.size());
		assertEquals(BigDecimal.valueOf(23.75), transfersAllUntil.get(0).getAmount());
		
		final List<Transfer> transfersOnlyOldest = transferService.findAllBetween(453850855, from.getTime(), until.getTime());
		assertEquals(2, transfersOnlyOldest.size());
		assertEquals(BigDecimal.valueOf(23.75), transfersOnlyOldest.get(0).getAmount());

	}

}
