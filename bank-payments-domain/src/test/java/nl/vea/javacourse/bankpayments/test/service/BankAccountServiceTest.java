package nl.vea.javacourse.bankpayments.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;

import nl.vea.javacourse.bankpayments.BalanceNotClearedException;
import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.repositories.BankAccountRepository;
import nl.vea.javacourse.bankpayments.repositories.CustomerRepository;
import nl.vea.javacourse.bankpayments.repositories.TransferRepository;
import nl.vea.javacourse.bankpayments.service.BankAccountService;
import nl.vea.javacourse.bankpayments.test.config.ApplicationTestContext;
import nl.vea.javacourse.bankpayments.test.util.PersistenceTestUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTestContext.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class BankAccountServiceTest {
	private static int counter = 0;
	
	@Resource
	BankAccountRepository bankAccountRepository;

	@Resource
	CustomerRepository customerRepository;
	
	@Resource
	TransferRepository transferRepository;

	@Inject
	private BankAccountService bankAccountService;

	@Autowired
	private DataSource datasource;

	@Before
	public void setUp() throws Exception {
		assertNotNull(datasource);
		if (counter == 0) {
			PersistenceTestUtils
			.executeSQL(
					datasource.getConnection(),
					Thread.currentThread()
							.getContextClassLoader()
							.getResourceAsStream(
									"ddl/Bank_Payments_DDL_HSQLDB.sql"));
		}
		
		PersistenceTestUtils.executeSQL(
				datasource.getConnection(),
				Thread.currentThread()
						.getContextClassLoader()
						.getResourceAsStream(
								"ddl/Bank_Payments_Inserts_HSQLDB.sql"));
		counter++;
	}

	@Test
	public void testCRUD() throws BankAccountNotFoundException, BalanceNotClearedException {
		assertNotNull(bankAccountRepository);
		assertNotNull(bankAccountService);
		assertEquals(8, bankAccountRepository.count());
		final BankAccount bankAccount_ID_2 = bankAccountService.findById(2L);
		final int accountNumber_312631316 = 312631316;
		assertEquals(accountNumber_312631316, bankAccount_ID_2.getAccountNumber());
		assertEquals("Willem", bankAccount_ID_2.getCustomer().getFirstName());
		assertEquals(BigDecimal.valueOf(-500.32), bankAccount_ID_2.getBalance());

		//test an unsuccessful  delete
		try {
			bankAccountService.delete(bankAccount_ID_2);
			fail("We expected a BalanceNotClearedException since the balance wasn't 0.00");
		} catch (BalanceNotClearedException e) {
			// Do nothing, let the test continue
			e.printStackTrace();
		}		
		
		// test update
		bankAccount_ID_2.setBalance(BigDecimal.valueOf(0.00));
		final BankAccount bankAccount_ID_2_v_2 = bankAccountService.update(bankAccount_ID_2);
		assertEquals(8, bankAccountRepository.count());
		assertEquals(BigDecimal.valueOf(0.00),
				bankAccount_ID_2_v_2.getBalance());
		
		//test a successful delete
		try{
			bankAccountService.delete(bankAccount_ID_2_v_2);
		} catch (Throwable t){
			t.printStackTrace();
		}
		final Customer willem = customerRepository.findOne(1L);
		assertEquals("Willem", willem.getFirstName());
		List<BankAccount> accountsWillem = bankAccountService.findAllForCustomer(willem);
		assertEquals(1, accountsWillem.size());
		assertEquals(453850855, accountsWillem.get(0).getAccountNumber());
		assertEquals(7, bankAccountRepository.count());
		assertNull( bankAccountService.findByAccountNumber(accountNumber_312631316));

		// test findByAccountNumber
		final BankAccount bankAccount_NR_114956154 = bankAccountService.findByAccountNumber(114956154);
		assertNotNull(bankAccount_NR_114956154);
		assertEquals(BigDecimal.valueOf(755.57), bankAccount_NR_114956154.getBalance());
		assertEquals("Bos", bankAccount_NR_114956154.getCustomer().getLastName());

		// test findAllForCustomer which is a convenient method
		// since bank accounts are fetched lazily when finding a customer with
		// the standard methods
		final Customer jan = customerRepository.findOne(2L);
		assertNotNull(jan);
		assertEquals("Plender", jan.getLastName());

		final List<BankAccount> accountsOfJan = bankAccountService
				.findAllForCustomer(jan);
		assertNotNull(accountsOfJan);
		assertEquals(2, accountsOfJan.size());

		// test createNewAccountFor
		final int newAccountNumber = 850294649;
		final Customer jacob = customerRepository.findOne(3L);
		final BankAccount bankAccount_NR_850294649 = bankAccountService
				.createNewAccountFor(newAccountNumber, jacob);
		assertNotNull(bankAccount_NR_850294649);
		assertEquals(8, bankAccountRepository.count());
		// assertEquals(9L, bankAccount_NR_850294649.getAccountId());
		assertEquals("Bos", bankAccount_NR_850294649.getCustomer()
				.getLastName());
		
		final BankAccount bankAccount_Latest = bankAccountService.createNewAccountFor(jacob.getCustomerId());
		assertNotNull(bankAccount_Latest);
		assertEquals("Bos", bankAccount_Latest.getCustomer()
				.getLastName());
		assertEquals(3L, bankAccount_Latest.getCustomer()
				.getCustomerId());
		
		assertEquals(9, bankAccountRepository.count());
		
		// another find method to get Customer Jacob with his accounts fetched
		// eagerly
		final Customer jacobWithAccountsFetched = customerRepository
				.findOneWithAccounts(3L);
		List<BankAccount> jacobsAccounts = jacobWithAccountsFetched
				.getAccounts();

		// assert that the new account is added to the customer side (the
		// inverse side)
		assertEquals(3, jacobsAccounts.size());
		boolean accountNR_850294649_IsFound = false;
		for (BankAccount bankAccount : jacobsAccounts) {
			accountNR_850294649_IsFound = bankAccount.getAccountNumber() == newAccountNumber;
			if (accountNR_850294649_IsFound) {
				break;
			}
		}
		assertTrue(accountNR_850294649_IsFound);

	}
	
	@Test
	public void testPayments() throws BankAccountNotFoundException{
		final BankAccount bankAccount_NR_312631316 = bankAccountService.findByAccountNumber(312631316);
		assertEquals(BigDecimal.valueOf(-500.32), bankAccount_NR_312631316.getBalance());
		final BankAccount bankAccount_NR_114956154 = bankAccountService.findByAccountNumber(114956154);
		assertEquals(BigDecimal.valueOf(755.57), bankAccount_NR_114956154.getBalance());
		assertEquals(5,transferRepository.count());
		final BigDecimal transferAmount_499_68 = BigDecimal.valueOf(499.68);
		try {
			Transfer transfer = bankAccountService.transfer(transferAmount_499_68, bankAccount_NR_312631316, bankAccount_NR_114956154, "grenswaarde 1000 creditlimiet");
			assertEquals(transferAmount_499_68, transfer.getAmount());
		} catch (OverdraftException e) {
			System.err.println(e.getMessage()); 
			e.printStackTrace();
		}
		assertEquals(6,transferRepository.count());
		final BankAccount bankAccount_NR_312631316_refresh_1 = bankAccountService.findByAccountNumber(312631316);
		assertEquals(BigDecimal.valueOf(-1000.0).setScale(7), bankAccount_NR_312631316_refresh_1.getBalance().setScale(7));
		final BankAccount bankAccount_NR_114956154_refresh_1 = bankAccountService.findByAccountNumber(114956154);
		assertEquals(BigDecimal.valueOf(1255.25), bankAccount_NR_114956154_refresh_1.getBalance());
		BigDecimal transferAmount_oneCent = BigDecimal.valueOf(0.01);
		try {
			bankAccountService.transfer(transferAmount_oneCent, bankAccount_NR_312631316, bankAccount_NR_114956154, "grenswaarde 1000 creditlimiet overschreden met 1 cent");
			fail("We expected an OverdraftException");
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage()); 
			e.printStackTrace();
		}
		assertEquals(6,transferRepository.count());
		final BankAccount bankAccount_NR_312631316_refresh_2 = bankAccountService.findByAccountNumber(312631316);
		assertEquals(BigDecimal.valueOf(-1000.0).setScale(1), bankAccount_NR_312631316_refresh_2.getBalance().setScale(1));
		final BankAccount bankAccount_NR_114956154_refresh_2 = bankAccountService.findByAccountNumber(114956154);
		assertEquals(BigDecimal.valueOf(1255.25), bankAccount_NR_114956154_refresh_2.getBalance());
		final BigDecimal transferAmount_1200_01 = BigDecimal.valueOf(1200.01);
		try {
			Transfer transfer = bankAccountService.transfer(transferAmount_1200_01, bankAccount_NR_114956154, bankAccount_NR_312631316, "1200.01 teruggestort ruim onder creditlimiet");
			assertEquals(transferAmount_1200_01, transfer.getAmount());
		} catch (OverdraftException e) {
			System.err.println(e.getMessage()); 
			e.printStackTrace();
		}
		assertEquals(7,transferRepository.count());
		final BankAccount bankAccount_NR_312631316_refresh_3 = bankAccountService.findByAccountNumber(312631316);
		assertEquals(BigDecimal.valueOf(200.01), bankAccount_NR_312631316_refresh_3.getBalance());
		final BankAccount bankAccount_NR_114956154_refresh_3 = bankAccountService.findByAccountNumber(114956154);
		assertEquals(BigDecimal.valueOf(55.24), bankAccount_NR_114956154_refresh_3.getBalance());
	}

}
