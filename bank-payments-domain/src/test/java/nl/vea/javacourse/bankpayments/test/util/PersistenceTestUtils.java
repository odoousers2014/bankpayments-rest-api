package nl.vea.javacourse.bankpayments.test.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to help with database related testing
 * 
 * @author NE72PG
 * 
 */
public class PersistenceTestUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceTestUtils.class);

	/**
	 * Utility method that can be used to prepare the database before running a test or clean it up after a test.
	 * 
	 * @param connection
	 *            used to create and execute statements
	 * @param is
	 *            containing the sql statements that should be executed
	 */
	public static void executeSQL(final Connection connection, final InputStream is) {
		final Scanner scanner = new Scanner(is);
		scanner.useDelimiter(";");
		try {
			connection.setAutoCommit(true);
			while (scanner.hasNext()) {
				final String sql = scanner.next();
				
				// second hasNext check to prevent the execution of any String fragment after the last ;
				// which won't constitute a valid sql statement and will lead to unnecessary SQLException logging
				if (scanner.hasNext()) {
					executeStatement(connection.createStatement(), sql);
				}
			}

		}
		catch (final SQLException e) {
			LOGGER.error(String.format("Failing to commit the connection: %s", connection), e);
		}
		finally {
			try {
				connection.close();
			}
			catch (final SQLException e) {
				LOGGER.error(String.format("Failing to close the connection: %s", connection), e);
			}
		}
	}

	private static void executeStatement(final Statement statement, final String sql) {
		try {
			LOGGER.debug(String.format("Trying to execute the following sql statement:\n %s", sql));
			statement.execute(sql);
		}
		catch (final SQLException e) {
			LOGGER.error(String.format("Failing to execute the following sql statement:\n %s", sql), e);
		}
		finally {
			try {
				statement.close();
			}
			catch (final SQLException e) {
				LOGGER.error(String.format("Failing to close the statement:\n %s", statement), e);
			}
		}
	}
}