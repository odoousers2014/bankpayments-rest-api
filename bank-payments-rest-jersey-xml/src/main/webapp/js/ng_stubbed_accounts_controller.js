﻿(function (app) {

	var stubbedAccountsController = function ($scope, stubbedAccountsFactory) {
		
		//feed stubbed data to the controller's scope
		$scope.stubbedAccounts = stubbedAccountsFactory.getStubbbedAccounts();
		console.log($scope.stubbedAccounts);
	};
	app.controller('stubbedAccountsController', ['$scope', 'stubbedAccountsFactory', stubbedAccountsController]);
})(angular.module('myApp')); // bestaande module doorgeven als parameter
