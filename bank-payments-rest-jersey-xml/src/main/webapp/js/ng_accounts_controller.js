﻿(function (app) {

	// 1. UITGEBREIDER voorbeeld met .success() en .error() callback
	var accountsController = function ($scope, $location, credentialsFactory, restFactory) {
		
		$scope.displayDetails = function (_url) {
			this.selectedUri = _url;
			//alert('selectedUri=' + this.selectedUri);
			console.log("selected url " + _url);
			//pass the url to the factory
			restFactory.selectedUrl = _url;
			//go to the account-details view/controller
			$location.path("/account-details");
		}
		
		restFactory.getResource('rest/bank-accounts')
			.then(
				// success function
				function (responseSuccess) {
					console.log(responseSuccess);
					credentialsFactory.setAuthenticated(true);
					$scope.accounts = responseSuccess.data;
				},
				function (responseError) {
					console.log(responseError);
					$scope.accounts = [];
					if (responseError.status === 401 /*&& $location.path() === "/live-accounts"*/) {
						credentialsFactory.setAuthenticated(false);
						$location.path("/login");
					}
					console.log($scope);
				}
			).then(function (responseSuccess) {
				
			});//end then
	};//end accountsController
		
	app.controller('accountsController', ['$scope', '$location','credentialsFactory', 'restFactory', accountsController]);
})(angular.module('myApp')); // bestaande module doorgeven als parameter
