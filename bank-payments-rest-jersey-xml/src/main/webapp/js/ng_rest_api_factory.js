﻿(function(app) {
	//Add a factory definition to the injected app module
	app.factory(
		'restFactory',['$http', 'credentialsFactory',
		function($http, credentialsFactory) {
			// 1. Maak een 'factory'-object
			var factory = {};
			
			//reusable local function
			factory.getResource = function(_url){
				var localHeaders = credentialsFactory.isAuthenticated() ?
						{
							'Authorization': "Basic " + credentialsFactory.getBasicAuthHeaderValue()
						} : {};
				return $http({
					method : 'get',
					url : _url,
					headers: localHeaders
				});
			}
			
			factory.selectedUrl = '';
			factory.getSelectedResource = function (){
				return this.getResource(this.selectedUrl);
			}

			// 4. Altijd tot slot: retourneer het factory-object
			return factory;
		}]);
})(angular.module('myApp'));