﻿(function (app) {

	// 1. UITGEBREIDER voorbeeld met .success() en .error() callback
	var accountDetailsController = function ($scope, $location, credentialsFactory, restFactory) {
		console.log("restFactory.selectedUrl=" + restFactory.selectedUrl);
		restFactory.getSelectedResource()
			.then(
				// success function
				function (responseSuccess) {
					console.log(responseSuccess);
					credentialsFactory.setAuthenticated(true);
					$scope.account = responseSuccess.data;
				},
				function (responseError) {
					console.log(responseError);
					$scope.account = {};
					if (responseError.status === 401 /*&& $location.path() === "/live-accounts"*/) {
						credentialsFactory.setAuthenticated(false);
						$location.path("/login");
					}
					console.log($scope);
				}
			).then(function (responseSuccess) {
				// ...doe nog iets anders.
				// define a function to view accountDetails
			});//end then
	};//end accountsController
		
	app.controller('accountDetailsController', ['$scope', '$location','credentialsFactory', 'restFactory', accountDetailsController]);
})(angular.module('myApp')); // bestaande module doorgeven als parameter
