/**
 * Is a singleton so the credentials should be the same throughout the application
 */
(function(app) {
	//Add a factory definition to the injected app module
	app.factory(
		'credentialsFactory',
		function() {
			// 1. Maak een 'factory'-object
			var factory = { "authenticated" : false, "credentials" : {"userName" : "", "password" : ""}};
			
			factory.setAuthenticated = function(isAuthenticated){
				this.authenticated = isAuthenticated;
			}
			
			factory.isAuthenticated = function(){
				return this.authenticated;
			}
			
			factory.setCredentials = function(userName, password) {
				this.credentials.userName = userName;
				this.credentials.password = password;
			}
			
			factory.getCredentials = function() {
				return this.credentials;
			}
			
			factory.getBasicAuthHeaderValue = function(){
				var result = 					 
			        btoa(this.credentials.userName + ":" + this.credentials.password);
			        console.log(result);
			        return result;
			}
			

			// 4. Altijd tot slot: retourneer het factory-object
			return factory;
		});
})(angular.module('myApp'));