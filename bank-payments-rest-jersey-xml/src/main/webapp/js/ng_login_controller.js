﻿(function (app) {

	// 1. UITGEBREIDER voorbeeld met .success() en .error() callback
	var loginController = function ($scope, $location, credentialsFactory) {
		$scope.userName = '';
		$scope.password = '';
		$scope.login = function(){
			credentialsFactory.setCredentials($scope.userName, $scope.password);
			credentialsFactory.setAuthenticated(true);//only locally not checked with backend service
			$location.path("/");
		}
			
	};//end accountsController
		
	app.controller('loginController', ['$scope', '$location','credentialsFactory', loginController]);
})(angular.module('myApp')); // bestaande module doorgeven als parameter
