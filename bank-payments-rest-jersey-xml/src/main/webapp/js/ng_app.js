﻿(function() {
	// Define the module in this file with 'ngRoute' a a submodule dependency
	var app = angular.module('myApp', [ 'ngRoute' ]);

	/*
	 * The custom "X-Requested-With" is a conventional header sent by browser clients,
	 *  and it used to be the default in Angular but they took it out in 1.3.0. Spring
	 *  Security responds to it by not sending a "WWW-Authenticate" header in a 401 response,
	 *  and thus the browser will not pop up an authentication dialog
	 *  (which is desirable in our app since we want to control the authentication).
	 */
	app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	}]);
	
	//see also http://stackoverflow.com/questions/17441254/why-angularjs-currency-filter-formats-negative-numbers-with-parenthesis
	// see also http://stackoverflow.com/questions/18612212/how-to-get-specific-currency-symbolrupee-symbol-in-my-case-in-angular-js-inste
	
	// decorator for negative signs instead of paranthesis
	app.config([ '$provide', function($provide) {
		$provide.decorator('$locale', [ '$delegate', function($delegate) {
			if ($delegate.id == 'en-us') {
				//switching comma and dot for dutch notation
				$delegate.NUMBER_FORMATS.DECIMAL_SEP = ',',
				$delegate.NUMBER_FORMATS.GROUP_SEP = '.',

				// expressing a negative amount with a minus sign
				$delegate.NUMBER_FORMATS.PATTERNS[1].negPre = '-\u00A4';
				$delegate.NUMBER_FORMATS.PATTERNS[1].negSuf = '';
			}
			return $delegate;
		} ]);
	} ]);
	
	// configuring view templates with their viewController to a URL
	// using the $routeProvider scope from the ngRoute module
	 app.config(function ($routeProvider) {
		 $routeProvider
			 .when('/', {
				 templateUrl: 'views/stubbed_accounts_view.html', // Default view
				 controller: 'stubbedAccountsController'
			 })
			 .when('/login', {
				templateUrl : 'views/login_view.html',
				controller : 'loginController'
			})
			 .when('/books', {
				 templateUrl: 'views/books_view.html', 
				 controller: 'bookController'
			 })
			 .when('/live-accounts', {
				 templateUrl: 'views/live_accounts_view.html',
				 controller: 'accountsController'
			 })
			 .when('/account-details', {
				 templateUrl: 'views/live_account_details_view.html',
				 controller: 'accountDetailsController'
			 })
			 .when('/stubbed-accounts', {
				 templateUrl: 'views/stubbed_accounts_view.html', // Default view
				 controller: 'stubbedAccountsController'
			 })
			 .otherwise({ redirectTo: '/' });
	 });
})();
