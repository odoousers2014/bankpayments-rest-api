﻿(function(app) {
	//Add a factory definition to the injected app module
	app.factory(
		'stubbedAccountsFactory',
		function() {
			// 1. Maak een 'factory'-object
			var factory = {};

			// define stubbed data,
			var stubbedAccounts = [ {
				"accountId" : 1,
				"accountNumber" : 453850855,
				"balance" : 850.56,
				"creditLimit" : 0,
				"customer" : {
					"customerId" : 1,
					"accountUserName" : "Willem",
					"firstName" : "Willem",
					"lastName" : "Es",
					"nameInsertion" : "van",
					"accounts" : [ 1, 2 ]
				}
			}, {
				"accountId" : 2,
				"accountNumber" : 312631316,
				"balance" : -50.33,
				"creditLimit" : 1000,
				"customer" : {
					"customerId" : 1,
					"accountUserName" : "Willem",
					"firstName" : "Willem",
					"lastName" : "Es",
					"nameInsertion" : "van",
					"accounts" : [ 1, 2 ]
				}
			}, {
				"accountId" : 3,
				"accountNumber" : 138272530,
				"balance" : 1250.12,
				"creditLimit" : 1500,
				"customer" : {
					"customerId" : 2,
					"accountUserName" : "Jan",
					"firstName" : "Jan",
					"lastName" : "Plender",
					"nameInsertion" : null,
					"accounts" : [ 3, 4 ]
				}
			}, {
				"accountId" : 4,
				"accountNumber" : 418854718,
				"balance" : 10201.9,
				"creditLimit" : 0,
				"customer" : {
					"customerId" : 2,
					"accountUserName" : "Jan",
					"firstName" : "Jan",
					"lastName" : "Plender",
					"nameInsertion" : null,
					"accounts" : [ 3, 4 ]
				}
			}, {
				"accountId" : 5,
				"accountNumber" : 114956154,
				"balance" : 755.57,
				"creditLimit" : 1000,
				"customer" : {
					"customerId" : 3,
					"accountUserName" : "Jacob",
					"firstName" : "Jacob",
					"lastName" : "Bos",
					"nameInsertion" : null,
					"accounts" : [ 5 ]
				}
			}, {
				"accountId" : 6,
				"accountNumber" : 132570459,
				"balance" : 647.22,
				"creditLimit" : 0,
				"customer" : {
					"customerId" : 4,
					"accountUserName" : "Alwin",
					"firstName" : "Alwin",
					"lastName" : "Ellenbroek",
					"nameInsertion" : null,
					"accounts" : [ 6, 7 ]
				}
			}, {
				"accountId" : 7,
				"accountNumber" : 653377991,
				"balance" : 241.38,
				"creditLimit" : 2000,
				"customer" : {
					"customerId" : 4,
					"accountUserName" : "Alwin",
					"firstName" : "Alwin",
					"lastName" : "Ellenbroek",
					"nameInsertion" : null,
					"accounts" : [ 6, 7 ]
				}
			}, {
				"accountId" : 8,
				"accountNumber" : 435453343,
				"balance" : 1047.73,
				"creditLimit" : 0,
				"customer" : {
					"customerId" : 5,
					"accountUserName" : "Andre",
					"firstName" : "André",
					"lastName" : "Lange",
					"nameInsertion" : "van",
					"accounts" : [ 8 ]
				}
			} ];

			// 3. Definieer functies als API/interface voor de
			// buitenwereld
			factory.getStubbbedAccounts = function() {
				return stubbedAccounts;
			};

			// 4. Altijd tot slot: retourneer het factory-object
			return factory;
		});
})(angular.module('myApp'));