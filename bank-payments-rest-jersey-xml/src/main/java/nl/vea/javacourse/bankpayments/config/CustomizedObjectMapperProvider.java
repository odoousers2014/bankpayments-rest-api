package nl.vea.javacourse.bankpayments.config;

import java.text.SimpleDateFormat;

import javax.ws.rs.core.Link;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import nl.vea.javacourse.bankpayments.config.domain.BankAccountMixIn;
import nl.vea.javacourse.bankpayments.config.domain.CustomerMixIn;
import nl.vea.javacourse.bankpayments.config.domain.LinkMixIn;
import nl.vea.javacourse.bankpayments.config.domain.TransferMixIn;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Provider that allows us to return a customized instance of {@link org.codehaus.jackson.map.ObjectMapper}. Using this
 * object mapper we can redefine the default Jackson behaviour and fine-tune how our JSON data structures look like.
 */
@Provider
public class CustomizedObjectMapperProvider implements
		ContextResolver<ObjectMapper> {
	
	//ISO-8601 Date-Timestamp
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS z";

	final ObjectMapper defaultObjectMapper;

	public CustomizedObjectMapperProvider() {
		defaultObjectMapper = createDefaultMapper();
	}

	@Override
	public ObjectMapper getContext(final Class<?> type) {
		return defaultObjectMapper;
	}

	private static ObjectMapper createDefaultMapper() {

		final ObjectMapper mapper = new ObjectMapper();

		// Use default pretty printer for JSON
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		// Configure date format used for serializing Date objects
		final SimpleDateFormat defaultDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		mapper.setDateFormat(defaultDateFormat);

		//By default all properties without explicit view definition are included in serialization
		//But we want to make it explicit for our configured ObjectMapper
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);

		// Properties without explicit view definition are NOT included in serialization.
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		// Provides Mixin functionality
		mapper.addMixIn(Customer.class, CustomerMixIn.class);
		mapper.addMixIn(BankAccount.class,BankAccountMixIn.class);		
		mapper.addMixIn(Transfer.class, TransferMixIn.class);
		mapper.addMixIn(Link.class, LinkMixIn.class);

		//mapper.addMixIn(JsonEntity.class, JsonEntityMixIn.class);

		return mapper;
	}
}
