package nl.vea.javacourse.bankpayments.responsehandling;

import java.io.Serializable;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;


public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = -6544194568263200491L;

	/** contains the same HTTP Status code returned by the server */
	private final int statusCode;	
	
	/** contains the  HTTP Status name returned by the server */
	private final String statusName;
	
	/** message describing the error*/
	private final String message;
	
	/** suggestion to the client for recovery*/
	private final String suggestion;
	
	/** extra information that might useful for developers */
	private final String developerMessage;	
	

	public ErrorMessage(int statusCode, String statusName, String message, String suggestion,
			String developerMessage) {
		super();
		this.statusCode = statusCode;
		this.statusName = statusName;
		this.message = message;
		this.suggestion = suggestion;
		this.developerMessage = developerMessage;
	}
	
	public ErrorMessage(Response.Status status, Throwable e, String suggestion,
			String developerMessage) {
		super();
		this.statusCode = status.getStatusCode();
		this.statusName = status.name();
		this.message = e.getMessage();
		this.suggestion = suggestion;
		this.developerMessage = developerMessage;
	}
	
	public ErrorMessage(StatusType unprocessableEntity, Throwable e, String suggestion,
			String developerMessage) {
		super();
		this.statusCode = unprocessableEntity.getStatusCode();
		this.statusName = unprocessableEntity.getReasonPhrase();
		this.message = e.getMessage();
		this.suggestion = suggestion;
		this.developerMessage = developerMessage;
	}


	public int getStatusCode() {
		return statusCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public String getMessage() {
		return message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public String getSuggestion() {
		return suggestion;
	}

}
