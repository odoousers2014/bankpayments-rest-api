package nl.vea.javacourse.bankpayments.config.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;

/**
 * Jackson annotation MixIn interface to add any required Jackson annotations to
 * the target class. The Target class {@link Transfer} is a JPA Entity from
 * the bank-payments-domain maven module this module is depending on for
 * database communication. Therefore this loosely annotation application is very convenient.
 * {@link CustomizedObjectMapperProvider} is responsible for binding this MixIn
 * interface to the target class.
 * 
 * @author Willem
 *
 */
//This annotation results in just ignoring all properties in a JSON message
//that cannot be mapped to any of the properties of the target class
@JsonIgnoreProperties(ignoreUnknown = true)
public interface TransferMixIn {
	
	@JsonView(Views.Basic.class)
	public long getTransferId();

	@JsonView(Views.Basic.class)
	public BankAccount getDonorAccount();

	@JsonView(Views.Basic.class)
	public BankAccount getBeneficiaryAccount();

	@JsonView(Views.Basic.class)
	public Date getTransferTimestamp(); 

	@JsonView(Views.Basic.class)
	public String getDescription();

	@JsonView(Views.Basic.class)
	public BigDecimal getAmount();


}
