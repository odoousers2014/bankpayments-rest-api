package nl.vea.javacourse.bankpayments.config.domain;

import java.util.List;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Jackson annotation MixIn interface to add any required Jackson annotations to
 * the target class. The Target class {@link Customer} is a JPA Entity from the
 * bank-payments-domain maven module this module is depending on for database
 * communication. Therefore this loosely annotation application is very
 * convenient. {@link CustomizedObjectMapperProvider} is responsible for binding
 * this MixIn interface to the target class.
 * 
 * @author Willem
 *
 */
// This annotation results in just ignoring all properties in a JSON message
// that cannot be mapped to any of the properties of the target class
@JsonIgnoreProperties(ignoreUnknown = true)
public interface CustomerMixIn {

	@JsonIgnore
	public String getAccountUserName();

	@JsonIgnore
	public void setAccountUserName(String accountUserName);

	@JsonView(Views.Basic.class)
	public String getFirstName();

	public void setFirstName(final String firstName);

	@JsonView(Views.Basic.class)
	public String getLastName();

	public void setLastName(final String lastName);

	@JsonView(Views.Basic.class)
	public String getNameInsertion();

	public void setNameInsertion(final String nameInsertion);

	@JsonView(Views.Detailed.class)
	// Using a serializer causes a report of
	// org.hibernate.collection.internal.PersistentBag cannot be cast to
	// nl.vea.javacourse.bankpayments.model.BankAccount (through reference
	// chain: nl.vea.javacourse.bankpayments.model.Customer["accounts"])
	//@JsonSerialize(using = BankAccountToIdandNumberSerializer.class)
	public List<BankAccount> getAccounts();

	public void setAccounts(final List<BankAccount> accounts);

	@JsonView(Views.Basic.class)
	public long getCustomerId();

}
