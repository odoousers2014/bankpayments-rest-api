package nl.vea.javacourse.bankpayments.config.domain;

import java.io.IOException;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.JsonEntity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

/**
 * This extension of {@link JsonSerializer} reduces a {@link Customer} object to
 * the value of its {@link Customer#getCustomerId()} and
 * {@link Customer#getFullName()}. This can be used by
 * {@link BankAccountMixIn#getCustomer()} by applying
 * <code>@JsonSerialize(using=CustomerToIdSerializer.class)</code> to prevent a
 * {@link StackOverflowError} as {@link Customer} and {@link BankAccount} are
 * JPA entities with a bidirectional relationship
 * 
 * @author Willem
 *
 */
public class CustomerToIdandNameSerializer extends JsonSerializer<Customer> {

	/**
	 * 
	 */
	@Override
	public void serialize(Customer customer, JsonGenerator jsonGenerator,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		jsonGenerator.writeStartObject();
		jsonGenerator.writeStringField("type", "customer");
		jsonGenerator.writeNumberField("customerId", customer.getCustomerId());
		jsonGenerator.writeStringField("firstName", customer.getFirstName());
		if (customer.getNameInsertion() != null) {
			jsonGenerator.writeStringField("nameInsertion",
					customer.getNameInsertion());
		}
		jsonGenerator.writeStringField("lastName", customer.getLastName());
		jsonGenerator.writeEndObject();
	}

	/**
	 * Since we introduced polymorphism with {@link Customer} implementing
	 * {@link JsonEntity} we also need to implement this method as it will be
	 * called instead of our earlier implementation of
	 * {@link #serialize(Customer, JsonGenerator, SerializerProvider)} See
	 * {@linkplain http
	 * ://stackoverflow.com/questions/27876027/json-jackson-exception
	 * -when-serializing-a-polymorphic-class-with-custom-serial}
	 */
	@Override
	public void serializeWithType(Customer customer,
			JsonGenerator jsonGenerator, SerializerProvider provider,
			TypeSerializer typeSer) throws IOException, JsonProcessingException {
		//typeSer.writeTypePrefixForObject(customer, jsonGenerator);
		//delegation to {@link #serialize(Customer, JsonGenerator, SerializerProvider)}
		serialize(customer, jsonGenerator, provider); 
		//typeSer.writeTypeSuffixForObject(customer, jsonGenerator);
	}

}
