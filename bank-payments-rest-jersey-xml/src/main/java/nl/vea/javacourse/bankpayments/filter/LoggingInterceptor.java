package nl.vea.javacourse.bankpayments.filter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class LoggingInterceptor implements ReaderInterceptor, WriterInterceptor {
	private static final Logger LOG = LoggerFactory
			.getLogger(LoggingInterceptor.class);

	@Context
	private UriInfo uriInfo;

	/**
	 * Intercepting the request to log the entity (in case of post)
	 */
	@Override
	public Object aroundReadFrom(ReaderInterceptorContext context)
			throws IOException, WebApplicationException {
		if (LOG.isDebugEnabled()) {
			MediaType media = context.getMediaType();
			String charset = media.getParameters().get(
					MediaType.CHARSET_PARAMETER);
			InputStream inputStream = context.getInputStream();
			byte[] inMemoryContent = readBytesIntoMemory(inputStream);
			if (charset != null) {
				LOG.debug("request body of {}:\n {}", uriInfo.getRequestUri(),
						new String(inMemoryContent, charset));
			} else {
				LOG.debug("request body of {}:\n {}", uriInfo.getRequestUri(),
						new String(inMemoryContent, "UTF-8"));
			}

			// As the previous InputStream from the ReaderInterceptorContext is
			// spent, provide it with a new one that can read from the
			// inMemoryContent
			ByteArrayInputStream buffer = new ByteArrayInputStream(
					inMemoryContent);
			context.setInputStream(buffer);

		}
		return context.proceed();
	}

	@Override
	public void aroundWriteTo(WriterInterceptorContext context)
			throws IOException, WebApplicationException {
		if (LOG.isDebugEnabled()) {
			MediaType media = context.getMediaType();
			String charset = media.getParameters().get(
					MediaType.CHARSET_PARAMETER);

			OutputStream oos = context.getOutputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			context.setOutputStream(baos);
			context.proceed();
			baos.flush();
			byte[] inMemoryContent = baos.toByteArray();
			// Do logging
			if (charset != null) {
				LOG.debug("response body of {}:\n {}", uriInfo.getRequestUri(),
						new String(inMemoryContent, charset));
			} else {
				LOG.debug("response body of {}:\n {}", uriInfo.getRequestUri(),
						new String(inMemoryContent));
			}

			// necessary to push the inMemoryContent to the client as well as
			// the logger
			oos.write(inMemoryContent);
		} else {
			context.proceed();
		}
	}

	private byte[] readBytesIntoMemory(InputStream is) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int read;
		byte[] data = new byte[1024];
		while ((read = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, read);
		}
		buffer.flush();
		return buffer.toByteArray();
	}

}
