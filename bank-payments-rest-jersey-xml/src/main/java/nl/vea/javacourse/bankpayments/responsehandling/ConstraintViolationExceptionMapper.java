package nl.vea.javacourse.bankpayments.responsehandling;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConstraintViolationExceptionMapper
		implements ExceptionMapper<ConstraintViolationException> {

	public ConstraintViolationExceptionMapper() {
	}

	@Override
	public Response toResponse(ConstraintViolationException exception) {
		// see http://www.restpatterns.org/HTTP_Status_Codes/409_-_Conflict
		return Response
				.status(Response.Status.CONFLICT)
				.entity(new ErrorMessage(
						Response.Status.CONFLICT,
						exception,
						"Check whether the amount is correct",
						"Invalid operation according to normal business rules; No error in programming, configuration or external systems"))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
