package nl.vea.javacourse.bankpayments.config.domain;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * At this time none of the concrete entities implement this interface as we would like to avoid 
 * a circular dependency, but we do want Jackson deserialization to work
 * If it is necessary we will move these to bank-payments-domain
 * after all we have the value objects over there as well
 * @author Willem
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = BankAccount.class, name = "bankAccount"),
		@Type(value = Customer.class, name = "customer"),
		@Type(value = Transfer.class, name = "transfer") })
@Deprecated
public interface JsonEntityMixIn {

}
