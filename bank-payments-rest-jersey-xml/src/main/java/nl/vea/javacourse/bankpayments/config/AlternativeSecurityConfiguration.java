package nl.vea.javacourse.bankpayments.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
/**
 * Enabling both basic authentication for url's with /rest/** and form authentication for everything else.
 * {@linkplain http://docs.spring.io/spring-security/site/docs/4.0.2.RELEASE/reference/htmlsingle/#multiple-httpsecurity}
 * This led to deploytime errors until I updated the spring.security.version in /bank-payments/pom.xml
 * Can be disabled in favor of {@link SecurityConfiguration} by commenting out @EnableWebSecurity and @Configuration annotations
 * @author Willem
 *
 */
@EnableWebSecurity
public class AlternativeSecurityConfiguration  {
	@Configuration
	@Order(1)
	public static class RestWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter{
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
					.antMatchers(HttpMethod.POST, "/rest/customers/**")
						.hasRole("ADMIN")
					.antMatchers("/rest/**")
						.authenticated()
					.and()
						.httpBasic()
					.and()
						.csrf().disable();
		}
	}

	@Configuration
	public static class FormLoginSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter{
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
					.antMatchers(HttpMethod.POST, "/customers/**")
						.hasRole("ADMIN")
					.anyRequest()
						.authenticated()
					.and()
						.formLogin()
					.and()
						.csrf().disable();
		}
	}
	
	
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth
			.inMemoryAuthentication()
				.withUser("user").password("user").roles("USER")
			.and()
				.withUser("admin").password("admin").roles("USER", "ADMIN")
			.and()
				.withUser("Alwin").password("Alwin").roles("USER")
			.and()
				.withUser("Jan").password("Jan").roles("USER")
			.and()
				.withUser("Willem").password("Willem").roles("USER")
			.and()
				.withUser("Jacob").password("Jacob").roles("USER")
			.and()
				.withUser("Andre").password("Andre").roles("USER")
			;
	}

}
