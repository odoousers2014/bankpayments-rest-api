package nl.vea.javacourse.bankpayments.config.domain;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import javax.ws.rs.core.Link;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * MixIn interface for {@link Link}
 * @author Willem
 *
 */
//This annotation results in just ignoring all properties in a JSON message
//that cannot be mapped to any of the properties of the target class
@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonDeserialize(using = LinkMixIn.LinkDeserializer.class)
public abstract class LinkMixIn extends Link {
	private static final Logger LOG = LoggerFactory
			.getLogger(LinkMixIn.class);
	
//	@JsonView(Views.Basic.class)
//	String getRel();
//	
//	@JsonIgnore
//	String[] getRels();
//	
//	@JsonView(Views.Extended.class)
//	String getTitle();
//	
//	@JsonView(Views.Extended.class)
//	String getType();
//	
//	@JsonIgnore
//	Map<String, String> getParams();
//	
//	@JsonIgnore
//	UriBuilder getUriBuilder();
//	
//	@JsonView(Views.Basic.class)
//	@JsonProperty("href")
//	URI getUri();
	
	private static final String HREF = "href";

    @JsonProperty(HREF)
    @Override
    public abstract URI getUri();

    @JsonAnyGetter
    public abstract Map<String, String> getParams();

    public static class LinkDeserializer extends JsonDeserializer<Link> {

        @Override
        public Link deserialize(
                final JsonParser p,
                final DeserializationContext ctxt) throws IOException {
        	LOG.info("Deserializing a Link");
            final Map<String, String> params = p.readValueAs(
                    new TypeReference<Map<String, String>>() {});
            if (params == null) {
                return null;
            }
            final String uri = params.remove(HREF);
            if (uri == null) {
                return null;
            }
            final Builder builder = Link.fromUri(uri);
            //params.forEach(builder::param);
            for (Map.Entry<String, String> entry : params.entrySet()) {
				builder.param(entry.getKey(), entry.getValue());
			}
            return builder.build();
        }
    }
	
}
