package nl.vea.javacourse.bankpayments.filter;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author adam-bien.com
 */
//@Tracked
@Provider
public class TrafficLogger implements ContainerRequestFilter, ContainerResponseFilter {

    private static final Logger LOG = LoggerFactory.getLogger(TrafficLogger.class.getName());

    public void filter(ContainerRequestContext requestContext) throws IOException {
        log(requestContext);
    }

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        log(responseContext);
    }

    void log(ContainerRequestContext requestContext) {
    	MultivaluedMap<String, String> headers = requestContext.getHeaders();
    	StringBuilder headersb = new StringBuilder("headers:\n");
    	for (String key : headers.keySet()) {
    		headersb.append(key);
    		headersb.append(" = ");
			int valueCounter = 0;
			for(String value: headers.get(key)){
				if(valueCounter > 0){
					headersb.append(", ");
				}
				headersb.append(value);
				valueCounter++;
			}
			headersb.append("\n");
		}
        String securityInfo = checkSecurityContext(requestContext);

        UriInfo uriInfo = requestContext.getUriInfo();
        String method = requestContext.getMethod();
        List<Object> matchedResources = uriInfo.getMatchedResources();
        String resources = "";
        for (Object resource : matchedResources) {
            resources += resource.getClass().getName();
        }
        LOG.debug("{} -> {}", uriInfo.getPath(), headersb.toString());
        LOG.debug("{}[{}] {} -> {}", securityInfo, method, uriInfo.getPath(), resources);
    }

	protected String checkSecurityContext(ContainerRequestContext requestContext) {
		SecurityContext securityContext = requestContext.getSecurityContext();
        String authentication = securityContext.getAuthenticationScheme();
        Principal userPrincipal = securityContext.getUserPrincipal();
        String securityInfo = "No authentication data available!";
        if (authentication != null || userPrincipal != null) {
            securityInfo = " Authentication: " + authentication + " Principal: " + userPrincipal + " ";
        }
		return securityInfo;
	}

    void log(ContainerResponseContext responseContext) {
        MultivaluedMap<String, String> stringHeaders = responseContext.getStringHeaders();
        Set<String> keys = stringHeaders.keySet();
        StringBuilder headers = new StringBuilder();
        for (String key : keys) {
            headers.append(key).append(":").append(stringHeaders.get(key));
        }
        String headerInfo = "";

        if (!keys.isEmpty()) {
            headerInfo = "Response: " + headers.toString();
        }
        Object entity = responseContext.getEntity();
        String entityInfo = " ";
        if (entity != null) {
            entityInfo = entity.getClass().getName();
        }
        LOG.debug("{} <- {}", headerInfo, entityInfo);
    }
}
