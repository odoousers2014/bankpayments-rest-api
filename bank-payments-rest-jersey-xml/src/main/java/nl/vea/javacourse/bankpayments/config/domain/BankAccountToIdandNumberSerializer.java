package nl.vea.javacourse.bankpayments.config.domain;

import java.io.IOException;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.JsonEntity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

/**
 * This extension of {@link JsonSerializer} reduces a {@link Customer} object to
 * the value of its {@link Customer#getCustomerId()} and
 * {@link Customer#getFullName()}. This can be used by
 * {@link BankAccountMixIn#getCustomer()} by applying
 * <code>@JsonSerialize(using=CustomerToIdSerializer.class)</code> to prevent a
 * {@link StackOverflowError} as {@link Customer} and {@link BankAccount} are
 * JPA entities with a bidirectional relationship
 * 
 * @author Willem
 *
 */
public class BankAccountToIdandNumberSerializer extends JsonSerializer<BankAccount> {

	/**
	 * 
	 */
	@Override
	public void serialize(BankAccount account, JsonGenerator jsonGenerator,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		jsonGenerator.writeStartObject();
		jsonGenerator.writeStringField("type", "bankAccount");
		jsonGenerator.writeNumberField("accountId", account.getAccountId());
		jsonGenerator.writeNumberField("accountNumber", account.getAccountNumber());
		jsonGenerator.writeEndObject();
	}

	/**
	 * Since we introduced polymorphism with {@link Customer} implementing
	 * {@link JsonEntity} we also need to implement this method as it will be
	 * called instead of our earlier implementation of
	 * {@link #serialize(Customer, JsonGenerator, SerializerProvider)} See
	 * {@linkplain http
	 * ://stackoverflow.com/questions/27876027/json-jackson-exception
	 * -when-serializing-a-polymorphic-class-with-custom-serial}
	 */
	@Override
	public void serializeWithType(BankAccount account,
			JsonGenerator jsonGenerator, SerializerProvider provider,
			TypeSerializer typeSer) throws IOException, JsonProcessingException {
		typeSer.writeTypePrefixForArray(account, jsonGenerator);
		//delegation to {@link #serialize(Customer, JsonGenerator, SerializerProvider)}
		serialize(account, jsonGenerator, provider); 
		typeSer.writeTypeSuffixForArray(account, jsonGenerator);
	}
}
