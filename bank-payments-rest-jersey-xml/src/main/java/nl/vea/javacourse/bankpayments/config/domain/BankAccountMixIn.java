package nl.vea.javacourse.bankpayments.config.domain;

import java.math.BigDecimal;

import nl.vea.javacourse.bankpayments.config.CustomizedObjectMapperProvider;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Jackson annotation MixIn interface to add any required Jackson annotations to
 * the target class. The Target class {@link BankAccount} is a JPA Entity from
 * the bank-payments-domain maven module this module is depending on for
 * database communication. Therefore this loosely annotation application is
 * very convenient. {@link CustomizedObjectMapperProvider} is responsible for
 * binding this MixIn interface to the target class.
 * 
 * @author Willem
 *
 */
// This annotation results in just ignoring all properties in a JSON message
// that cannot be mapped to any of the properties of the target class
@JsonIgnoreProperties(ignoreUnknown = true)
public interface BankAccountMixIn {
	// Besides ignoring properties not present in the target class all property
	// names are identical so no additional annotations are necessary.
	
	@JsonView(Views.Basic.class)
	public int getAccountNumber();

	public void setAccountNumber(int accountNumber);

	@JsonView(Views.Detailed.class)
	public BigDecimal getBalance();

	public void setBalance(BigDecimal balance);

	@JsonView(Views.Detailed.class)
	public BigDecimal getCreditLimit();

	public void setCreditLimit(BigDecimal creditLimit);

	/*
	 * {@link CustomerToIdandNameSerializer} prevents exposing {@link Customer#getAccounts()}
	 * as that will lead to a Droste effect during serialization resulting in {@link StackOverflowError}
	 * @return
	 */
	@JsonView(Views.Extended.class)
	@JsonSerialize(using=CustomerToIdandNameSerializer.class)
	public Customer getCustomer();

	@JsonIgnore
	public void setCustomer(Customer customer);

	@JsonView(Views.Basic.class)
	public long getAccountId();

}
