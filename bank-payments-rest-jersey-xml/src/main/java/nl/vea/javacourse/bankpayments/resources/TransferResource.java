package nl.vea.javacourse.bankpayments.resources;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.config.domain.Views;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.responsehandling.ResourceNotFoundException;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.responsehandling.UnauthorizedException;
import nl.vea.javacourse.bankpayments.service.BankAccountService;
import nl.vea.javacourse.bankpayments.service.TransferService;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;

@Component
@Path("/transfers")
public class TransferResource {
	private static final Logger LOG = LoggerFactory.getLogger(TransferResource.class);
	@Inject
	TransferService transferService;

	@Inject
	BankAccountService bankAccountService;

	public TransferResource() {
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createTransferBetweenAccounts(@Context UriInfo uriInfo,
	/*
	 * @Context HttpServletRequest servletRequest, is a nullpointer in a JerseyTest
	 */
	@Context SecurityContext securityContext, Transfer transfer)
			throws BankAccountNotFoundException, OverdraftException, UnauthorizedException {
		// beware the Transfer is only partially filled
		final String userName = securityContext.getUserPrincipal().getName();
		LOG.debug(String.format("The name of the User Principal is %s", userName));
		BankAccount donor = bankAccountService
				.findByAccountNumber(transfer.getDonorAccount().getAccountNumber());
		if (donor == null) {
			throw new BankAccountNotFoundException(transfer.getDonorAccount().getAccountNumber());
		}
		String accountUserName = donor.getCustomer().getAccountUserName();
		if (!userName.equals(accountUserName)) {
			LOG.error(String
					.format("The user principal name is '%s' and the account user name of the corresponding customer in the database is '%s'",
							userName, accountUserName));
			throw new UnauthorizedException(userName, transfer.getDonorAccount().getAccountNumber());
		}
		BankAccount beneficiary = bankAccountService.findByAccountNumber(transfer
				.getBeneficiaryAccount().getAccountNumber());
		if (beneficiary == null) {
			throw new BankAccountNotFoundException(transfer.getBeneficiaryAccount().getAccountNumber());
		}
		Transfer result = bankAccountService.transfer(transfer.getAmount(), donor, beneficiary,
				transfer.getDescription());

		URI resourcePath = UriBuilder.fromResource(TransferResource.class)
				.path(TransferResource.class, "getTransferById")
				.build(String.valueOf(result.getTransferId()));
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location,
						"The transfer has successfully completed.")).contentLocation(resourcePath)
				.header("location", location).build();
	}

	@GET
	@Path("bank-account-nr/{accountNumber}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Basic.class)
	public List<Transfer> getTransfersByBankAccount(
			@PathParam("accountNumber") Integer accountNumber) {
		
		return transferService.findAllByAccountNumber(accountNumber);
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Basic.class)
	public Transfer getTransferById(@PathParam("id") Long id)
			throws ResourceNotFoundException {
		Transfer result = transferService.findById(id);
		if (result == null) {
			throw new ResourceNotFoundException("Transfer", "transferId", id.toString());
		}
		
		return transferService.findById(id);
	}

	// method to test behavior of UriInfo and UriBuilder
	@GET
	@Path("echo-path/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getPathInfo(@Context HttpServletRequest servletRequest,
			@Context UriInfo uriInfo, @PathParam("id") Long id) throws ResourceNotFoundException {
		URI echoPath = uriInfo.getAbsolutePath();
		URI resourcePath = UriBuilder.fromResource(BankAccountResource.class)
				.path(BankAccountResource.class, "getBankAccountById").build("5");
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.OK)
				// 201
				.entity(new ResponseMessage(Response.Status.OK, echoPath.toString(),
						"absolute URI for bank-account with id = 5: " + location))
				.contentLocation(echoPath).header("location", echoPath).build();
	}
}
