package nl.vea.javacourse.bankpayments.resources;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import nl.vea.javacourse.bankpayments.CustomerNotFoundException;
import nl.vea.javacourse.bankpayments.config.domain.Views;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.responsehandling.ResourceNotFoundException;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.service.BankAccountService;
import nl.vea.javacourse.bankpayments.service.CustomerService;
import nl.vea.javacourse.bankpayments.vo.BankAccountVO;
import nl.vea.javacourse.bankpayments.vo.CustomerVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;

@Component
@Path("/customers")
public class CustomerResource {

	@Autowired
	CustomerService customerService;
	
	@Autowired
	BankAccountService bankAccountService;
	
	public CustomerResource() {
		// TODO Auto-generated constructor stub
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Extended.class)
	public List<Customer/*VO*/> getCustomers() throws IOException {
		return customerService.findAll();
		//return CustomerVO.getVOfromEntities(customerService.findAll());
	}
	
	@GET
	@Path("{id}/bank-accounts")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BankAccountVO>  getBankAccountsOfCustomer(@PathParam("id") Long id) throws IOException, ResourceNotFoundException
			 {
		List<BankAccount> accounts = bankAccountService.findAllForCustomerId(id);
		if (accounts == null || accounts.isEmpty()) {
			throw new ResourceNotFoundException("bank-accounts", "customer-id", id.toString());
		}
		return BankAccountVO.getVOfromEntities(accounts);
	}
	
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Detailed.class)
	public Customer/*VO*/ getCustomerById(@PathParam("id") Long id) throws IOException, ResourceNotFoundException {
		Customer customer = customerService.findById(id);
		if (customer == null) {
			throw new ResourceNotFoundException("customer", "customer-id", id.toString());
		}
		//return new CustomerVO(customer);
		return customer;
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createCustomer(@Context UriInfo uriInfo, CustomerVO customerVO){
		Customer customer = customerService.createNewCustomer(customerVO);
		URI resourcePath = UriBuilder.fromResource(CustomerResource.class).path(CustomerResource.class, "getCustomerById").build(String.valueOf(customer.getCustomerId()));		
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location, "A new customer has been created"))
				.contentLocation(resourcePath)
				.header("location", location)
				.build();
	}
	
	//TODO update the response with nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage and javax.ws.rs.core.UriBuilder
	@POST
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomer(@Context HttpServletRequest servletRequest,@PathParam("id") Long id, CustomerVO customerVO) throws CustomerNotFoundException{
		
		Customer customer = customerService.updateCustomer(customerVO);
		
		return Response.accepted().status(Response.Status.PARTIAL_CONTENT).contentLocation(URI.create(servletRequest.getContextPath() + "/customers/" + customer.getCustomerId())).build();
	}
	
	@POST
	@Path("{id}/bank-accounts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createBankAccountForExistingCustomer(
			@Context UriInfo uriInfo,
			@PathParam("id") Long id, BankAccountVO bankAccountVO) throws IOException {
		BankAccount bankAccount = bankAccountService
				.createNewAccountFor(id, bankAccountVO);
		URI resourcePath = UriBuilder.fromResource(BankAccountResource.class).path(BankAccountResource.class, "getBankAccountById").build(String.valueOf(bankAccount.getAccountId()));		
		String location = uriInfo.getBaseUri().toString() + resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location, "A new bank account has been created"))
				.contentLocation(resourcePath)
				.header("location", location)
				.build();
	}
	
}
