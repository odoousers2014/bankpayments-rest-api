package nl.vea.javacourse.bankpayments.responsehandling;

import java.io.Serializable;

import javax.ws.rs.core.Response;

public class ResponseMessage implements Serializable{
	
	private static final long serialVersionUID = 7293051482439688936L;

	/** contains the same HTTP Status code returned by the server */
	private final int statusCode;	
	
	/** contains the  HTTP Status name returned by the server */
	private final String statusName;
	
	private final String location;
	
	private final String message;

	public ResponseMessage(final int statusCode, final String statusName, final String location, final String message) {
		super();
		this.statusCode = statusCode;
		this.statusName = statusName;
		this.location = location;
		this.message = message;
	}

	public ResponseMessage(Response.Status status, String location, final String message) {
		super();
		this.statusCode = status.getStatusCode();
		this.statusName = status.name();
		this.location = location;
		this.message = message;
	}
	
	public ResponseMessage(final int statusCode, final String statusName, final String location) {
		this(statusCode, statusName, location, "");
	}

	public ResponseMessage(final Response.Status status, final String location) {
		this(status, location, "");
	}
	
	public ResponseMessage(){
		this(200, "OK", "");
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public String getLocation() {
		return location;
	}

	public String getMessage() {
		return message;
	}
	
}
