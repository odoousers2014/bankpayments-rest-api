package nl.vea.javacourse.bankpayments.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Can be disabled in favor of {@link AlternativeSecurityConfiguration} by commenting out @EnableWebSecurity and @Configuration annotations
 * @author Willem
 *
 */
//@Configuration
//@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/customers/**").hasRole("ADMIN")
		.anyRequest()
		.authenticated()		
		.and()
		.httpBasic()
		.and()
		.csrf()
		.disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		auth
			.inMemoryAuthentication()
				.withUser("user").password("user").roles("USER")
			.and()
				.withUser("admin").password("admin").roles("USER", "ADMIN")
			.and()
				.withUser("Alwin").password("Alwin").roles("USER")
			.and()
				.withUser("Jan").password("Jan").roles("USER")
			.and()
				.withUser("Willem").password("Willem").roles("USER")
			.and()
				.withUser("Jacob").password("Jacob").roles("USER")
			.and()
				.withUser("Andre").password("Andre").roles("USER")
			;
	}

}
