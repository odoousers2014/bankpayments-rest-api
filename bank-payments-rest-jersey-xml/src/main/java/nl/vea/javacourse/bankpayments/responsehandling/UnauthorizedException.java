package nl.vea.javacourse.bankpayments.responsehandling;

public class UnauthorizedException extends Exception {

	private static final long serialVersionUID = 1394479844389746200L;
	private static final String MSG = "The bank account with number %s you chose to transfer money from is not registered in your name";
	private final String userName;
	private final Integer accountNumber;
	public UnauthorizedException(final String arg0, final String userName, final Integer accountNumber) {
		super(arg0);
		this.userName = userName;
		this.accountNumber = accountNumber;
	}
	
	public UnauthorizedException(final String userName, final Integer accountNumber) {
		super(String.format(MSG, accountNumber));
		this.userName = userName;
		this.accountNumber = accountNumber;
	}
	public String getUserName() {
		return userName;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
}
