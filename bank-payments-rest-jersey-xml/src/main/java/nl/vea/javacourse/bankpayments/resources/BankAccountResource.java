package nl.vea.javacourse.bankpayments.resources;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import nl.vea.javacourse.bankpayments.BankAccountNotFoundException;
import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.config.domain.EntityWrapper;
import nl.vea.javacourse.bankpayments.config.domain.Views;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.responsehandling.ResourceNotFoundException;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.responsehandling.UnauthorizedException;
import nl.vea.javacourse.bankpayments.service.BankAccountService;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;

@Component
@Path("/bank-accounts")
public class BankAccountResource {
	private static final Logger LOG = LoggerFactory
			.getLogger(BankAccountResource.class);

	@Autowired
	BankAccountService bankAccountService;

	public BankAccountResource() {
	}

	@POST
	@Path("create-for-customer")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.TEXT_HTML })
	public Response createBankAccountForExistingCustomer(
			@Context UriInfo uriInfo,
			@Context HttpServletRequest servletRequest,
			@FormParam("customer-id") Long customerId) throws IOException {
		BankAccount bankAccount = bankAccountService
				.createNewAccountFor(customerId);

		URI resourcePath = UriBuilder.fromResource(BankAccountResource.class)
				.path(BankAccountResource.class, "getBankAccountById")
				.build(String.valueOf(bankAccount.getAccountId()));
		String location = uriInfo.getBaseUri().toString()
				+ resourcePath.getPath().substring(1);
		return Response
				.status(Response.Status.CREATED)
				// 201
				.entity(new ResponseMessage(Response.Status.CREATED, location,
						"The transfer has successfully completed."))
				.contentLocation(resourcePath).header("location", location)
				.build();
	}



	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Extended.class)
	public List<BankAccount> getBankAccounts(@Context UriInfo uriInfo)
			throws IOException {
		// retrieving the entities
		final List<BankAccount> accounts = bankAccountService.findAll();

		// constructing all EntityWrapper<BankAccount> objects with relevant
		// link information
		// final List<BankAccount> result = new ArrayList<>();
		final UriBuilder builderById = uriInfo.getAbsolutePathBuilder().path(
				getClass(), "getBankAccountById");

		LOG.debug(uriInfo.getAbsolutePath().toString());
		LOG.debug(builderById.toTemplate());

		// Adding link information works with serialization to JSON
		// but gives deserialization problems:
		// javax.ws.rs.ProcessingException: Error reading entity from input
		// stream.
		//
		// Caused by: com.fasterxml.jackson.databind.JsonMappingException: Can
		// not instantiate abstract type [simple type, class
		// javax.ws.rs.core.Link] (need to add/enable type information?)
		for (final BankAccount bankAccount : accounts) {
			final List<Link> links = new ArrayList<>();
			Link detailsLink = Link
					.fromUri(builderById.build(bankAccount.getAccountId()))
					.rel("details")
					.param("customParameter", "Diederik Duck")
					.title(String.format("accountId=%s",
							bankAccount.getAccountId())).build();
			links.add(detailsLink);
			bankAccount.addLink(detailsLink);
			// result.add(new EntityWrapper<BankAccount>(links,bankAccount));
			// LOG.debug(builderById.build(bankAccount.getAccountId()).toString());

		}
		return accounts;
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Detailed.class)
	public BankAccount getBankAccountById(@PathParam("id") Long id)
			throws IOException, ResourceNotFoundException {
		BankAccount bankAccount = bankAccountService.findById(id);
		if (bankAccount == null) {
			throw new ResourceNotFoundException("bank-account",
					"bank-account-id", id.toString());
		}
		return bankAccount;
	}

	@GET
	@Path("account-nr/{account-nr}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@JsonView(Views.Detailed.class)
	public BankAccount getBankAccountByAccountNumber(@Context SecurityContext securityContext, @Context UriInfo uriInfo,
			@PathParam("account-nr") Integer accountNumber) throws IOException,
			ResourceNotFoundException {
		
		LOG.debug(uriInfo.getRequestUri().toString());
		LOG.debug(uriInfo.getPath());
		
		//Temporary code for research purposes
		try {
			final String userName = securityContext.getUserPrincipal().getName();
			LOG.debug(String.format("The name of the User Principal is %s",
					userName));
		} catch (Throwable e) {
			LOG.error("Some exception occcurred trying to access the security context", e);
		}
		
		BankAccount bankAccount = bankAccountService
				.findByAccountNumber(accountNumber);
		if (bankAccount == null) {
			throw new ResourceNotFoundException("bank-account", "account-nr",
					accountNumber.toString());
		}
		final UriBuilder builderById = uriInfo.getAbsolutePathBuilder().path(
				getClass(), "getBankAccountById");
		Link detailsLink = Link
				.fromUri(builderById.build(bankAccount.getAccountId()))
				.rel("self")
				.param("customParameter", "Diederik Duck")
				.title(String.format("accountId=%s",
						bankAccount.getAccountId())).build();
		bankAccount.addLink(detailsLink);
		return bankAccount;
	}

}
