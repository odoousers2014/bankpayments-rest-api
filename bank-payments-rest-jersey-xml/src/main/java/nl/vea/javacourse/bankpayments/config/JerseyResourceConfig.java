package nl.vea.javacourse.bankpayments.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;


/**
 * Ultimately inherits from {@link javax.ws.rs.core.Application}
 * used to register the JAX-RS resources and providers
 */
@ApplicationPath("/*")
public class JerseyResourceConfig extends ResourceConfig{

	public JerseyResourceConfig() {
		packages(true,"nl.vea.javacourse.bankpayments");
	}

}
