package nl.vea.javacourse.bankpayments.config.domain;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.ws.rs.core.UriBuilder;

import nl.vea.javacourse.bankpayments.model.JsonEntity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


 /**
  * Wrapper to add HATEOAS link information to the entity being wrapped
  * @author Willem
  *
  * @param <T> type of the Entity being wrapped, This, however, complicates 
  * JSON deserialization with Jacskon
  */
@Deprecated
public class EntityWrapper<T extends JsonEntity> {
	
	private final List<Link> _links;
	
	//for this to work with Jackson  deserialization we need to add polymorphism
	private final T data;
	
	@JsonCreator
	public EntityWrapper(@JsonProperty("_links") final List<Link> _links, @JsonProperty("data") final T data){
		this._links = _links;
		this.data = data;
	}

	public List<Link> get_links() {
		return _links;
	}
	
//	public String getType(){
//		return data.getClass().getSimpleName();
//	}

	public T getData() {
		return data;
	}
	
	/**
	 * Attempt to device a generic method to convert a list of entities of type E to a list of the corresponding EntityWrapper<E> elements.
	 * The problem is that it isn't possible to derive the necessary specifics to create the corresponding Links here
	 * @param uriBuilder
	 * @param list
	 * @return
	 */
	public static <E extends JsonEntity> List<EntityWrapper<E>> convertList(final UriBuilder uriBuilder, final List<E> list){
		final List<EntityWrapper<E>> wrapperlist = new ArrayList<>(list.size());
		for (E entry : list) {
			//wrapperlist.add(new EntityWrapper<>(entry.getValue(),entry.getKey()));
		}
		return null;
	}
}
