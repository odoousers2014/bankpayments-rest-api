package nl.vea.javacourse.bankpayments.responsehandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import nl.vea.javacourse.bankpayments.OverdraftException;

@Provider
public class OverdraftExceptionMapper implements
		ExceptionMapper<OverdraftException> {

	public OverdraftExceptionMapper() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Response toResponse(OverdraftException exception) {
		
		// see http://www.restpatterns.org/HTTP_Status_Codes/409_-_Conflict
		// or http://www.restpatterns.org/HTTP_Status_Codes/422_-_Unprocessable_Entity seems more appropriate, but is not supported within 
		// the enum javax.ws.rs.core.Response.Status
		//anonymous Inner class
		StatusType unprocessableEntity = new StatusType(){

			@Override
			public int getStatusCode() {
				return 422;
			}

			@Override
			public Family getFamily() {
				return Family.CLIENT_ERROR;
			}

			@Override
			public String getReasonPhrase() {
				return "Unprocessable Entity";
			}
			
		};
		
		return Response
				.status(unprocessableEntity)
				.entity(new ErrorMessage(
						unprocessableEntity,
						exception,
						String.format(
								"The maximal amount that can still be transferred is \u20ac %.2f until the account is replenished.",
								exception.getMaximumAllowed()),
						"Invalid operation according to normal business rules; No error in programming, configuration or external systems"))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
