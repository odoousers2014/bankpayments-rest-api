package nl.vea.javacourse.bankpayments.responsehandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {

	public UnauthorizedExceptionMapper() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Response toResponse(UnauthorizedException exception) {
		// see http://www.restpatterns.org/HTTP_Status_Codes/401_-_Unauthorized
		return Response
				.status(Response.Status.UNAUTHORIZED)
				.entity(new ErrorMessage(
						Response.Status.UNAUTHORIZED,
						exception,
						String.format(
								"Check whether the account number's value %s is correct and that the username %s you logged in with os correct",
								exception.getAccountNumber(), exception.getUserName()),
						String.format(
								"Verify whether the customer with the user name %s should have a corresponding bank account with number %s in the database",
								exception.getUserName(), exception.getAccountNumber())))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
