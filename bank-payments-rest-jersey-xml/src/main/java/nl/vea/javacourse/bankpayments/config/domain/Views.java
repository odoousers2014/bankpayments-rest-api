package nl.vea.javacourse.bankpayments.config.domain;

/**
 * Definitions of JsonViews that can help to discern between basic properties to be serialized
 * in a list view and extended properties that should be serialized when zooming in on the details of
 * one element of the list.
 * {@linkplain http://wiki.fasterxml.com/JacksonJsonViews}
 * @author Willem
 *
 */
public class Views {
	
	/**
	 * represents the collection of properties that should always be visible (no details) 
	 * @author Willem
	 *
	 */
	public static class Basic{
		
	}
	
	/**
	 * represents the collection of properties that should be visible in an extended view (a few extra details) 
	 * @author Willem
	 *
	 */
	public static class Extended  extends Basic{
	}

	/**
	 * represents the collection of properties that should only be visible in a detailed view (all extra details)
	 * @author Willem
	 *
	 */
	public static class Detailed extends Extended {
	}
}
