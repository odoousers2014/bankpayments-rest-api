package nl.vea.javacourse.bankpayments.responsehandling;

public class ResourceNotFoundException extends Exception{

	private static final long serialVersionUID = -6782515532307782350L;
	
	private static final String MSG =  "The  %s resource with a %s value of %s was not found.";
	
	private final String entityName;
	
	private final String searchAttributeName;
	
	private final String searchAttributeValue;

	public ResourceNotFoundException(String entityName,
			String searchAttributeName, String searchAttributeValue) {
		super(String.format(MSG, entityName, searchAttributeName, searchAttributeValue));
		this.entityName = entityName;
		this.searchAttributeName = searchAttributeName;
		this.searchAttributeValue = searchAttributeValue;
	}

	public String getEntityName() {
		return entityName;
	}

	public String getSearchAttributeName() {
		return searchAttributeName;
	}

	public String getSearchAttributeValue() {
		return searchAttributeValue;
	}
	
}
