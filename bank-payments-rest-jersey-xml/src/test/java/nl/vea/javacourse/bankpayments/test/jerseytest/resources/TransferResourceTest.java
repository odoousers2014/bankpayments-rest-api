package nl.vea.javacourse.bankpayments.test.jerseytest.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.resources.BankAccountResource;
import nl.vea.javacourse.bankpayments.resources.TransferResource;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest;
import nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseyTestSpringContext;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * This is an integration test that combines {@link JerseyTest} with a {@link ApplicationContext} by
 * extending {@link JerseySpringTest}. This way we can combine DI of Spring components with
 * JAX-RS/Jersey specific configurations. The class under test is {@link TransferResource} and
 * the test integrates the whole chain from client side calls to JAX-RS resource method and includes
 * the database access layer calling an in-memory database.
 * 
 * @author Willem
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JerseyTestSpringContext.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class TransferResourceTest extends JerseySpringTest {

	private static final Logger LOG = LoggerFactory.getLogger(TransferResourceTest.class);

	@Override
	protected ResourceConfig configure() {
		// returning just the Resource class under test
		// The super class will add other important components to the returned
		// ResourceConfig instance
		return new ResourceConfig(TransferResource.class);
	}
	
	
	@Before
	@Override
	public void setup() throws Exception {
		// Very important as the super class sets up the database.
		super.setup();
	}


	@After
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}



	/**
	 * This test requires registering
	 * {@link nl.vea.javacourse.bankpayments.test.jerseytest.filter.SecurityContextEnricher} in
	 * {@link 
	 * nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest.setApplicationContext(
	 * ...).new JerseyTest() ...}.configure()} for the user principal from the basic authentication
	 * to be available from the SecurityContext parameter of the
	 * {@link BankAccountResource#createTransferBetweenAccounts(javax.ws.rs.core.UriInfo, javax.ws.rs.core.SecurityContext, TransferVO)}
	 * method under test.
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void testTransferOverdraft() {
		final HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Jacob", "Jacob");
		final String path = "/transfers";
		final WebTarget postTarget = target(path).register(feature);
		Transfer transfer = new Transfer(
				new BankAccount(114956154), // donor
				new BankAccount(312631316), // beneficiary				
				"nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest.testTransferOverdraft()",
				BigDecimal.valueOf(1755.58) // balance + credit limit = 1755.57
				);
		final Response postResponse = postTarget.request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
		try {
			if (Response.Status.CREATED.getStatusCode() == postResponse.getStatus()) {
				fail("We expected the REST service to warn that there was an overdraft.");
			} else {
				assertEquals(422, postResponse.getStatusInfo().getStatusCode());
				LOG.info(String
						.format("We tried to submit a money transfer POST request on %s,\n but we got status code %s, %s with the following content %s ",
								path, postResponse.getStatusInfo().getStatusCode(), postResponse
										.getStatusInfo().getReasonPhrase(), postResponse
										.readEntity(String.class)));
			}
		} finally {
			postResponse.close();
		}
	}

	/**
	 * This test requires registering
	 * {@link nl.vea.javacourse.bankpayments.test.jerseytest.filter.SecurityContextEnricher} in
	 * {@link 
	 * nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest.setApplicationContext(
	 * ...).new JerseyTest() ...}.configure()} for the user principal from the basic authentication
	 * to be available from the SecurityContext parameter of the
	 * {@link BankAccountResource#createTransferBetweenAccounts(javax.ws.rs.core.UriInfo, javax.ws.rs.core.SecurityContext, TransferVO)}
	 * method under test.
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void testTransferWrongCustomer() {
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Willem", "Willem");

		final String path = "/transfers";
		final WebTarget postTarget = target(path).register(feature);
		Transfer transfer = new Transfer(
				new BankAccount(418854718), // donor
				new BankAccount(312631316), // beneficiary				
				"nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest.testTransferWrongCustomer()",
				BigDecimal.valueOf(5012.55) // balance + credit limit = 1755.57
				);
		final Response postResponse = postTarget.request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
		try {
			if (Response.Status.CREATED.getStatusCode() == postResponse.getStatus()) {
				fail("We expected the REST service to warn that the UserPrincipal was not authorized to use the donor account.");
			} else {
				assertEquals(401, postResponse.getStatusInfo().getStatusCode());
				LOG.info(String
						.format("We tried to submit a money transfer POST request on %s,\n but we got status code %s, %s with the following content %s ",
								path, postResponse.getStatusInfo().getStatusCode(), postResponse
										.getStatusInfo().getReasonPhrase(), postResponse
										.readEntity(String.class)));
			}
		} finally {
			postResponse.close();
		}
	}

	/**
	 * This test requires registering
	 * {@link nl.vea.javacourse.bankpayments.test.jerseytest.filter.SecurityContextEnricher} in
	 * {@link 
	 * nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest.setApplicationContext(
	 * ...).new JerseyTest() ...}.configure()} for the user principal from the basic authentication
	 * to be available from the SecurityContext parameter of the
	 * {@link BankAccountResource#createTransferBetweenAccounts(javax.ws.rs.core.UriInfo, javax.ws.rs.core.SecurityContext, TransferVO)}
	 * method under test.
	 * 
	 * @throws JsonProcessingException
	 */
	@Ignore
	@Test
	public void testTransferNegativeAmount() {
		final HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Jan", "Jan");
		final String path = "/transfers";
		final WebTarget postTarget = target(path).register(feature);
		Transfer transfer = new Transfer(
				new BankAccount(418854718), // donor
				new BankAccount(312631316), // beneficiary				
				"nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest.testTransferNegativeAmount()",
				BigDecimal.valueOf(-2.55) 
				);

		final Response postResponse = postTarget.request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
		try {
			if (Response.Status.CONFLICT.getStatusCode() == postResponse.getStatus()) {
				// To be able to read information from the response more than
				// once to get a different format
				// response.bufferEntity();
				ResponseMessage message = postResponse.readEntity(ResponseMessage.class);
				assertNotNull(message);

				// Try to retrieve the transfer that was just made

			} else {
				LOG.error(String
						.format("We tried to submit a money transfer POST request on %s,\n but we got status code %s, %s with the following content %s ",
								path, postResponse.getStatusInfo().getStatusCode(), postResponse
										.getStatusInfo().getReasonPhrase(), postResponse
										.readEntity(String.class)));
				// TODO currently the test fails as we don't get any TransactionSystemException or
				// ConstraintViolationException
				fail(String.format("The status was not %s but %s, %s", Response.Status.CONFLICT
						.getStatusCode(), postResponse.getStatusInfo().getStatusCode(),
						postResponse.getStatusInfo().getReasonPhrase()));
			}
		} finally {
			postResponse.close();
		}
	}

	/**
	 * This test requires registering
	 * {@link nl.vea.javacourse.bankpayments.test.jerseytest.filter.SecurityContextEnricher} in
	 * {@link 
	 * nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest.setApplicationContext(
	 * ...).new JerseyTest() ...}.configure()} for the user principal from the basic authentication
	 * to be available from the SecurityContext parameter of the
	 * {@link BankAccountResource#createTransferBetweenAccounts(javax.ws.rs.core.UriInfo, javax.ws.rs.core.SecurityContext, TransferVO)}
	 * method under test.
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void testSuccessfullTransfer() throws JsonProcessingException {
		final HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Jan", "Jan");
		final String path = "/transfers";
		final WebTarget postTarget = target(path).register(feature);
		Transfer transfer = new Transfer(
				new BankAccount(418854718), // donor
				new BankAccount(312631316), // beneficiary				
				"nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest.testSuccessfullTransfer()",
				BigDecimal.valueOf(5012.55) 
				);
		
		final Response postResponse = postTarget.request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
		try {
			if (Response.Status.CREATED.getStatusCode() == postResponse.getStatus()) {
				// To be able to read information from the response more than
				// once to get a different format
				// response.bufferEntity();
				ResponseMessage message = postResponse.readEntity(ResponseMessage.class);
				assertNotNull(message);

				// Try to retrieve the transfer that was just made
				final WebTarget getTarget = target(message.getLocation().substring(
						message.getLocation().indexOf("/transfers")));
				final Response getResponse = getTarget.request(MediaType.APPLICATION_JSON_TYPE)
						.get();
				final Transfer retrievedTransfer = getResponse.readEntity(Transfer.class);
				assertNotNull(retrievedTransfer);
				assertEquals((long) transfer.getDonorAccount().getAccountNumber(), (long) retrievedTransfer
						.getDonorAccount().getAccountNumber());
				assertEquals((long) transfer.getBeneficiaryAccount().getAccountNumber(),
						(long) retrievedTransfer.getBeneficiaryAccount().getAccountNumber());
				assertEquals(transfer.getAmount(), retrievedTransfer.getAmount());
				assertEquals(transfer.getDescription(), retrievedTransfer.getDescription());
				assertEquals(
						Long.parseLong(message.getLocation().substring(
								message.getLocation().lastIndexOf("/") + 1)),
						retrievedTransfer.getTransferId());
			} else {
				LOG.error(String
						.format("We tried to submit a money transfer POST request on %s,\n but we got status code %s, %s with the following content %s ",
								path, postResponse.getStatusInfo().getStatusCode(), postResponse
										.getStatusInfo().getReasonPhrase(), postResponse
										.readEntity(String.class)));
				fail(String.format("The status was not %s but %s, %s", Response.Status.CREATED
						.getStatusCode(), postResponse.getStatusInfo().getStatusCode(),
						postResponse.getStatusInfo().getReasonPhrase()));
			}
		} finally {
			postResponse.close();
		}
	}

}
