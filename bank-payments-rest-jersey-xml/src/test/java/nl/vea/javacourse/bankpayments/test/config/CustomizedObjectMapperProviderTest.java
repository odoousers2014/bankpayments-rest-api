package nl.vea.javacourse.bankpayments.test.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.ws.rs.ext.ContextResolver;

import nl.vea.javacourse.bankpayments.OverdraftException;
import nl.vea.javacourse.bankpayments.config.CustomizedObjectMapperProvider;
import nl.vea.javacourse.bankpayments.config.domain.BankAccountMixIn;
import nl.vea.javacourse.bankpayments.config.domain.CustomerMixIn;
import nl.vea.javacourse.bankpayments.config.domain.CustomerToIdandNameSerializer;
import nl.vea.javacourse.bankpayments.config.domain.EntityWrapper;
import nl.vea.javacourse.bankpayments.config.domain.TransferMixIn;
import nl.vea.javacourse.bankpayments.config.domain.Views;
import nl.vea.javacourse.bankpayments.config.domain.Views.Detailed;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Customer;
import nl.vea.javacourse.bankpayments.model.Transfer;

import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CustomizedObjectMapperProviderTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(CustomizedObjectMapperProviderTest.class);
	private ContextResolver<ObjectMapper> contextResolver = new CustomizedObjectMapperProvider();

	@Test
	public void testMixInPresent() {

		assertEquals(4, contextResolver.getContext(Object.class).mixInCount());

		@SuppressWarnings("rawtypes")
		Class mixInForCustomerClass = contextResolver.getContext(Object.class)
				.findMixInClassFor(Customer.class);
		assertEquals(CustomerMixIn.class.getName(),
				mixInForCustomerClass.getName());
	}

	/**
	 * Asserts comparing JSON Strings with serialized Java Objects are
	 * difficult: one difference in order, formatting etc. will break the test
	 * Here we are mainly interested in asserting that the serializations to
	 * JSON does not lead to a {@link StackOverflowError} being thrown. As the
	 * Jackson MixIn interfaces should prevent exposing both sides of the
	 * bidirectional relationships of their corresponding JPA entities. To this
	 * end {@link BankAccountMixIn#getCustomer()} is annotated with
	 * {@link JsonSerialize#using()} to register
	 * {@link CustomerToIdandNameSerializer}
	 * 
	 * @throws JsonProcessingException
	 * @throws OverdraftException
	 */
	@Test
	public void testSerialization() throws JsonProcessingException,
			OverdraftException {

		// Fully instantiated bidirectional relationship
		final Customer willem = new Customer(1L, "willem", "Willem", "Es",
				"van");
		final BankAccount account_123456 = new BankAccount(123456,
				BigDecimal.valueOf(552.25), willem);
		final List<BankAccount> accountsWillem = new ArrayList<>();
		accountsWillem.add(account_123456);
		willem.setAccounts(accountsWillem);
		final Customer Jan = new Customer(2L, "Jan", "Jan", "Plender", null);
		final BankAccount account_654321 = new BankAccount(654321,
				BigDecimal.valueOf(552.25), Jan);
		final List<BankAccount> accountsJan = new ArrayList<>();
		accountsJan.add(account_654321);
		Jan.setAccounts(accountsJan);
		// Jan's 536.69 saldo wordt Willem's saldo wordt 567.81
		final Transfer transfer_15_56 = account_654321
				.payTo(BigDecimal.valueOf(15.56), account_123456,
						"Jan betaalt Willem");

		// should not lead to StackOverflowError because the MixIns are
		// configured to ignore one side of the bidirectional relationship
		// here BankAccountMixIn ignores customer as property
		LOG.info(contextResolver.getContext(Object.class)
				.writerWithView(Views.Basic.class).writeValueAsString(Jan));
		LOG.info(contextResolver.getContext(Object.class)
				.writerWithView(Views.Detailed.class)
				.writeValueAsString(account_654321));
		LOG.info(contextResolver.getContext(Object.class)
				.writerWithView(Views.Basic.class).writeValueAsString(willem));
		LOG.info(contextResolver.getContext(Object.class)
				.writerWithView(Views.Detailed.class)
				.writeValueAsString(account_123456));
		LOG.info(contextResolver.getContext(Object.class)
				.writerWithView(Views.Extended.class)
				.writeValueAsString(transfer_15_56));

	}

	@Test
	public void testLinkSerialization() throws JsonProcessingException {
		// Fully instantiated bidirectional relationship
		final Customer willem = new Customer(1L, "willem", "Willem", "Es",
				"van");
		final BankAccount account_123456 = new BankAccount(123456,
				BigDecimal.valueOf(552.25), willem);
		final Link link = createLink("details", "account-nr=654321", "bank-accounts/account-nr", "123456");
		account_123456.addLink(link);
		final List<BankAccount> accountsWillem = new ArrayList<>();
		accountsWillem.add(account_123456);
		willem.setAccounts(accountsWillem);

		final String jsonWillem = contextResolver.getContext(Object.class)
				.writerWithView(Views.Detailed.class).writeValueAsString(willem);
		LOG.info(jsonWillem);
		try {
			final Customer deserialized = contextResolver.getContext(Object.class).readerFor(Customer.class).readValue(jsonWillem);
			assertNotNull(deserialized);
			assertEquals("Willem", deserialized.getFirstName());
			final List<BankAccount> accounts = deserialized.getAccounts();
			assertNotNull(accounts);
			assertEquals(1, accounts.size());
			final BankAccount account = accounts.get(0);
			assertNotNull(account);
			assertEquals(123456, account.getAccountNumber());
			assertEquals(BigDecimal.valueOf(552.25), account.getBalance());
			final List<Link> links =account.get_links();
			assertNotNull(links);
			assertEquals(1, links.size());
			final Link retrievedlink = links.get(0);
			assertEquals(link.getUri(), retrievedlink.getUri());
		} catch (IOException e) {
			LOG.warn("Something went wrong deserializing a customer", e);
			fail("Deserializing the customer failed.");
		}
		
	}
	
	private Link createLink(final String rel, final String title, final String resource, final String param){
		final Link link = Link
				.fromUri(
						"https://localhost:8443/bank-payments-rest-jersey-xml/rest/{resource}/{param}")
				.rel(rel)
				.type("application/json")
				.title(title)
				.build(resource, param);
		return link;
	}
}
