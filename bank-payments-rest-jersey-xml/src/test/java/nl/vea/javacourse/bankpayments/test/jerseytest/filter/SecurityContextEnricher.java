package nl.vea.javacourse.bankpayments.test.jerseytest.filter;

import java.security.Principal;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;

import nl.vea.javacourse.bankpayments.filter.TrafficLogger;

/**
 * 
 * Extension of {@link TrafficLogger} that facilitates enriching the {@link SecurityContext}
 * instance coupled to the HTTP Request in test scenarios with user principal information available
 * in the 'authorization' request header since the JerseyTest providers neglect to do so. In this
 * way the JAX-RS Resource methods under test can consult their SecurityContext parameter as they
 * would under real production circumstances.
 * 
 * The Logging functionality of the superclass is retained.
 * 
 * As we only want to use this filter in a JerseyTest environment we do not wish to annotate them
 * with {@link Provider}, the warning from the JAX-RS validator should therefore be ignored.
 * 
 * @author Willem
 *
 */
public class SecurityContextEnricher extends TrafficLogger {

	/**
	 * When {@link SecurityContext} provided by {@link ContainerRequestContext#getSecurityContext()}
	 * has no user principal a new SecurityContext instance is created with a user principal derived
	 * from the basic authentication request header (if present).
	 * 
	 * @return message returning information from the SecurityContext instance, which the superclass
	 *         will use for logging purposes
	 */
	@Override
	protected String checkSecurityContext(final ContainerRequestContext requestContext) {

		// Get the authentification passed in HTTP headers parameters
		final String auth = requestContext.getHeaderString("authorization");

		// If the user does not have the right (does not provide any HTTP Basic Auth)
		if (auth == null) {
			return "No authentication data available!";
		}

		// lap : loginAndPassword
		final String[] lap = BasicAuth.decode(auth);

		// If login or password fail
		if (lap == null || lap.length != 2) {
			return "authorization header could not be processed";
		}
		if (requestContext.getSecurityContext() == null
				|| requestContext.getSecurityContext().getUserPrincipal() == null) {

			requestContext.setSecurityContext(new SecurityContext() {

				@Override
				public Principal getUserPrincipal() {
					return new Principal() {

						@Override
						public String getName() {
							return lap[0];
						}

					};
				}

				@Override
				public boolean isUserInRole(String role) {
					// Not under test
					return true;
				}

				@Override
				public boolean isSecure() {
					// Not under test
					return true;
				}

				@Override
				public String getAuthenticationScheme() {
					if (lap != null && lap.length == 2) {
						return SecurityContext.BASIC_AUTH;
					} else {
						return "UNKNOWN";
					}
				}

			});
		}
		return " Authentication: " + requestContext.getSecurityContext().getAuthenticationScheme()
				+ " Principal: " + requestContext.getSecurityContext().getUserPrincipal().getName()
				+ " ";
	}

	/**
	 * Allow to encode/decode the authentification
	 * 
	 * @author Deisss (LGPLv3)
	 */
	private static class BasicAuth {
		/**
		 * Decode the basic auth and convert it to array login/password
		 * 
		 * @param auth
		 *            The string encoded authentification
		 * @return The login (case 0), the password (case 1)
		 */
		public static String[] decode(String auth) {
			// Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
			auth = auth.replaceFirst("[B|b]asic ", "");

			// Decode the Base64 into byte[]
			byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

			// If the decode fails in any case
			if (decodedBytes == null || decodedBytes.length == 0) {
				return null;
			}

			// Now we can convert the byte[] into a splitted array :
			// - the first one is login,
			// - the second one password
			return new String(decodedBytes).split(":", 2);
		}
	}

}
