package nl.vea.javacourse.bankpayments.test.client.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nl.vea.javacourse.bankpayments.config.CustomizedObjectMapperProvider;
import nl.vea.javacourse.bankpayments.config.domain.Views;
import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This integration test relies on a manually deployed war file on Tomcat and therefore can not be
 * used as a part of the Maven build process. Moreover, the Oracle 11g XE 'production' DB needs to
 * be up and running.
 * 
 * Besides this type of post-deployment test there are tests based on
 * {@link org.glassfish.jersey.test.JerseyTest} that can be run during Maven builds. See
 * {@link BankAccountResourceTest}
 * 
 * @author Willem
 *
 */
public class BankAccountResourceIT {

	private static final Logger LOG = LoggerFactory.getLogger(BankAccountResourceIT.class);

	private static Client client;

	/**
	 * As the Client and ClientBuilder are heavy weight but thread safe objects we can resuse them
	 * over several tests (or in production environment multiple request/response runs
	 * 
	 * No SSL configuration to the ClientBuilder was necessary. We had two issues, though: 1)
	 * javax.ws.rs.ProcessingException: javax.net.ssl.SSLHandshakeException:
	 * java.security.cert.CertificateException: No name matching localhost found 2) After that was
	 * resolved sun.security.validator.ValidatorException: PKIX path building failed:
	 * sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid
	 * certification path to requested target The first could be resolved by adding localhost as
	 * SubjectAlternativeName to the keypair (actually by recreating the keypair) The second was
	 * resolved by exporting the keypair as base64 encoded X.509 v3 certificate from the browser and
	 * importing it in the truststore with the keytool. The location of the truststore is revealed
	 * by debugging using the JVM parameter -Djavax.net.debug=ssl:handshake {@linkplain http://java.globinch
	 * .com/enterprise-java/security/fix-java-security-certificate-exception-no-matching
	 * -localhost-found/} {@linkplain http://java.globinch.com/enterprise-java/security/pkix-path-building
	 * -failed-validation-sun-security-validatorexception/}
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Jan", "Jan");
		client = ClientBuilder.newBuilder().register(JacksonFeature.class).register(feature)
				.register(CustomizedObjectMapperProvider.class).build();
	}

	/**
	 * Never forget to close the Client object
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		client.close();
	}

	@Ignore
	@Test
	public void testGetBankAccountById() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBankAccounts() throws JsonProcessingException {
		final String url = "https://localhost:8443/bank-payments-rest-jersey-xml/rest/bank-accounts";
		//final String url = "https://localhost:8443/bank-payments-rest-jersey-xml-0.0.1-SNAPSHOT/rest/bank-accounts";
		Response response = null;
		try {
			response = client.target(url).request(MediaType.APPLICATION_JSON_TYPE).get();
			if (Response.Status.OK.getStatusCode() == response.getStatus()) {
				// To be able to read information from the response more than
				// once to get a different format
				// response.bufferEntity();
				List<BankAccount> bankAccounts = response
						.readEntity(new GenericType<List<BankAccount>>() {
						});
				assertNotNull(bankAccounts);
				assertTrue(
						"The sql insert script provides 7 bank_account records (but more could be inserted afterward)",
						bankAccounts.size() > 6);
				LOG.debug(String
						.format("We were able to retrieve a bank account from a GET request on %s\n with the following content\n %s ",
								url, bankAccounts));
			} else {
				LOG.error(String
						.format("We trying to retrieve a bank account from a GET request on %s,\n but we got status code %s, %s with the following content %s ",
								url, response.getStatusInfo().getStatusCode(), response
										.getStatusInfo().getReasonPhrase(), response
										.readEntity(String.class)));
				fail(String.format("The status was not 200 but %s, %s", response.getStatusInfo()
						.getStatusCode(), response.getStatusInfo().getReasonPhrase()));
			}

		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Test
	public void testGetBankAccountByAccountNumber() throws JsonProcessingException {
		final String url = "https://localhost:8443/bank-payments-rest-jersey-xml/rest/bank-accounts/account-nr/312631316";
		//final String url = "https://localhost:8443/bank-payments-rest-jersey-xml-0.0.1-SNAPSHOT/rest/bank-accounts";
		Response response = null;
		try {
			response = client.target(url).request(MediaType.APPLICATION_JSON_TYPE).get();
			if (Response.Status.OK.getStatusCode() == response.getStatus()) {
				BankAccount bankAccount_312631316 = response.readEntity(BankAccount.class);
				assertNotNull(bankAccount_312631316);
				assertEquals(312631316L, bankAccount_312631316.getAccountNumber());
			} else {
				LOG.error(String
						.format("We are trying to retrieve a bank account from a GET request on %s,\n but we got status code %s, %s with the following content %s ",
								url, response.getStatusInfo().getStatusCode(), response
										.getStatusInfo().getReasonPhrase(), response
										.readEntity(String.class)));
				fail(String.format("The status was not 200 but %s, %s", response.getStatusInfo()
						.getStatusCode(), response.getStatusInfo().getReasonPhrase()));
			}

		} finally {
			if (response != null) {
				response.close();
			}
		}
	}
}
