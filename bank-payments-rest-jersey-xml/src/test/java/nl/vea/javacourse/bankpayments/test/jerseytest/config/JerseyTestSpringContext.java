package nl.vea.javacourse.bankpayments.test.jerseytest.config;

//import nl.vea.javacourse.bankpayments.test.config.ApplicationTestContext;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;

@Configuration
//The explicit import doesn't seem necessary as long as the @ComponentScan is present
//@Import(ApplicationTestContext.class)
@ComponentScan(basePackages = { "nl.vea.javacourse.bankpayments" })
public class JerseyTestSpringContext {

}
