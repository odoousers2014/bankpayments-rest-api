package nl.vea.javacourse.bankpayments.test.jerseytest.config;

import static org.junit.Assert.assertNotNull;

import javax.sql.DataSource;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;

import nl.vea.javacourse.bankpayments.config.CustomizedObjectMapperProvider;
import nl.vea.javacourse.bankpayments.filter.LoggingInterceptor;
import nl.vea.javacourse.bankpayments.resources.BankAccountResource;
import nl.vea.javacourse.bankpayments.resources.CustomerResource;
import nl.vea.javacourse.bankpayments.resources.TransferResource;
import nl.vea.javacourse.bankpayments.responsehandling.OverdraftExceptionMapper;
import nl.vea.javacourse.bankpayments.responsehandling.UnauthorizedExceptionMapper;
import nl.vea.javacourse.bankpayments.test.jerseytest.filter.SecurityContextEnricher;
import nl.vea.javacourse.bankpayments.test.util.PersistenceTestUtils;

import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyWebTarget;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Extend this class in a spring test framework enabled JUnit test instead of {@link JerseyTest} to
 * be able to combine the Jersey test framework with the Spring TestContext framework. In this way
 * the subclass can be an integration test that combines JAX-RS/Jersey specific configurations with
 * DI of all necessary Spring components from the declared {@link ApplicationContext}.
 * 
 * For this magic to work we had to let this class implement {@link ApplicationContextAware} with
 * its {@link ApplicationContextAware#setApplicationContext(ApplicationContext)}. This method passes
 * the {@link ApplicationContext} configured in the {@link @ContextConfiguration} annotation of the
 * subclass to the associated {@link #_jerseyTest}
 * 
 * @author Willem
 *
 */
public abstract class JerseySpringTest implements ApplicationContextAware {
	private static final Logger LOG = LoggerFactory.getLogger(JerseySpringTest.class);	
	private static int counter = 0;
	private JerseyTest _jerseyTest;

	@Autowired
	private DataSource datasource;
	
	public final WebTarget target(final String path) {
		JerseyWebTarget target = (JerseyWebTarget) _jerseyTest.target(path);

		// has to be added to use it at the client side for JSON deserialization
		// e.g. for the benefit of javax.ws.rs.core.Response.readEntity(Class<T> entityType)
		target.register(CustomizedObjectMapperProvider.class);
		return target;
	}

	public Client client() {
		JerseyClient client = (JerseyClient) _jerseyTest.client();

		// has to be added to use it at the client side for JSON deserialization
		// e.g. for the benefit of javax.ws.rs.core.Response.readEntity(Class<T> entityType)
		client.register(CustomizedObjectMapperProvider.class);
		return client;
	}

	@Before
	public void setup() throws Exception {
		_jerseyTest.setUp();
		
		// Database definition is only necessary the first time
		if (counter == 0) {
			assertNotNull(datasource);
			PersistenceTestUtils.executeSQL(datasource.getConnection(), Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream("ddl/Bank_Payments_DDL_HSQLDB.sql"));
		}
		// truncates / inserts
		PersistenceTestUtils.executeSQL(datasource.getConnection(), Thread.currentThread()
				.getContextClassLoader()
				.getResourceAsStream("ddl/Bank_Payments_Inserts_HSQLDB.sql"));
		counter++;

	}

	@After
	public void tearDown() throws Exception {
		_jerseyTest.tearDown();
	}

	/**
	 * This method takes care of setting the Jersey {@link ResourceConfig} (that is produced by the
	 * subclass implementation of {@link #configure()} ) with the {@link ApplicationContext} that
	 * the subclass JUnit test has declared to use with a {@link @ContextConfiguration} annotation.
	 */
	@Autowired
	public void setApplicationContext(final ApplicationContext context) {
		_jerseyTest = new JerseyTest() {

			@Override
			protected Application configure() {
				enable(TestProperties.LOG_TRAFFIC);
				enable(TestProperties.DUMP_ENTITY);

				/*
				 * retrieving the ResourceConfig object that is produced by a subclass instance of
				 * this abstract class, which will already contain configurations set within the subclass
				 */
				ResourceConfig application = JerseySpringTest.this.configure()

				/*
				 * adding only specific classes to the Jersey runtime to keep it as lean as possible
				 * such as the CustomizedObjectMapperProvider, which is the
				 * ContextResolver<ObjectMapper> providing the Jackson ObjectMapper with the
				 * necessary MixIn interfaces. This is more selective than .packages(true,
				 * "nl.vea.javacourse.bankpayments"), which enables the Jersey runtime to discover
				 * all annotated providers within this package structure during its provider
				 * scanning phase. Beware that the provider is only registered for the server side
				 * of the test setup.
				 */
				.register(CustomizedObjectMapperProvider.class)				
				.register(SecurityContextEnricher.class, Priorities.AUTHENTICATION)
				.register(LoggingInterceptor.class)
				
				//add all exception mappers associated with the test scenarios
				.register(UnauthorizedExceptionMapper.class)
				.register(OverdraftExceptionMapper.class)

				// adding the Spring ApplicationContext
				.property("contextConfig", context);

				// Do some config verification
				final String template = "is {} registered? {}";
				LOG.info(template, CustomizedObjectMapperProvider.class.getName(),
						application.isRegistered(CustomizedObjectMapperProvider.class));

				// should be true when running
				// nl.vea.javacourse.bankpayments.test.jerseytest.resources.BankAccountResourceTest
				// as it is added in its configure() implementation.
				LOG.info(template, BankAccountResource.class.getName(),
						application.isRegistered(BankAccountResource.class));
				LOG.info(template, CustomerResource.class.getName(),
						application.isRegistered(CustomerResource.class));
				LOG.info(template, TransferResource.class.getName(),
						application.isRegistered(TransferResource.class));
				return application;
			}

		};
	}

	/**
	 * Must be implemented by the subclass to create a {@link ResourceConfig} registered with all
	 * the resource classes to be tested
	 * 
	 * @return
	 */
	protected abstract ResourceConfig configure();

}
