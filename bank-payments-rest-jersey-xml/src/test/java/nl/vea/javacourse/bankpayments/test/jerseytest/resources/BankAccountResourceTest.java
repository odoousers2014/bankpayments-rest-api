package nl.vea.javacourse.bankpayments.test.jerseytest.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.List;

import javax.sql.DataSource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nl.vea.javacourse.bankpayments.model.BankAccount;
import nl.vea.javacourse.bankpayments.model.Transfer;
import nl.vea.javacourse.bankpayments.resources.BankAccountResource;
import nl.vea.javacourse.bankpayments.resources.TransferResource;
import nl.vea.javacourse.bankpayments.responsehandling.ResponseMessage;
import nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseySpringTest;
import nl.vea.javacourse.bankpayments.test.jerseytest.config.JerseyTestSpringContext;
import nl.vea.javacourse.bankpayments.test.util.PersistenceTestUtils;
import nl.vea.javacourse.bankpayments.vo.TransferVO;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * This is an integration test that combines {@link JerseyTest} with a {@link ApplicationContext} by
 * extending {@link JerseySpringTest}. This way we can combine DI of Spring components with
 * JAX-RS/Jersey specific configurations. The class under test is {@link BankAccountResource} and
 * the test integrates the whole chain from client side calls to JAX-RS resource method and includes
 * the database access layer calling an in-memory database.
 * 
 * @author Willem
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JerseyTestSpringContext.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class BankAccountResourceTest extends JerseySpringTest {

	private static final Logger LOG = LoggerFactory.getLogger(BankAccountResourceTest.class);
	
	/**
	 * Setup the table inserts before each test method
	 */
	@Before
	public void setup() throws Exception {
		// Don't forget to call the super classes setup
		super.setup();
		

	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected ResourceConfig configure() {

		// returning just the Resource class under test
		// The super class will add other important components to the returned
		// ResourceConfig instance
		return new ResourceConfig(BankAccountResource.class);
	}

	@Test
	public void testGetBankAccountByAccountNumber() throws JsonProcessingException {
		// Client somehow needs an absolute URI a WebTarget doesn't
		final String path = "http://localhost:9998" + "/bank-accounts/account-nr/312631316";
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("Jan", "Jan");

		Client client = client().register(feature);
		LOG.debug("HttpAuthenticationFeature with basic credentials is registered: {}", client
				.getConfiguration().isRegistered(feature));
		WebTarget target = client.target(path)/* .register(feature) */;
		final Response response = target.request(MediaType.APPLICATION_JSON_TYPE).get();
		LOG.debug("HTTP Status code {}", response.getStatus());
		LOG.debug(response.getStatusInfo().getReasonPhrase());
		try {
			if (Response.Status.OK.getStatusCode() == response.getStatus()) {

				BankAccount bankAccount_312631316 = response.readEntity(BankAccount.class);
				assertNotNull(bankAccount_312631316);
				assertEquals(312631316L, bankAccount_312631316.getAccountNumber());
				assertEquals(2, bankAccount_312631316.getAccountId());
				assertEquals(1000.00, bankAccount_312631316.getCreditLimit().doubleValue(), 0);
				assertEquals(BigDecimal.valueOf(-500.32), bankAccount_312631316.getBalance());
				assertEquals(1L, bankAccount_312631316.getCustomer().getCustomerId());
				assertEquals("Es", bankAccount_312631316.getCustomer().getLastName());
				assertEquals("Willem", bankAccount_312631316.getCustomer().getFirstName());
				assertEquals("van", bankAccount_312631316.getCustomer().getNameInsertion());
				assertNull(
						"Service doesn't provide transitive accounts information of the associated Customer",
						bankAccount_312631316.getCustomer().getAccounts());
				assertNull(
						"Service doesn't provide accountUserName information of the associated Customer for security reasons",
						bankAccount_312631316.getCustomer().getAccountUserName());
				LOG.debug(String
						.format("We were able to retrieve a bank account from a GET request on %s\n with the following content\n %s ",
								path, bankAccount_312631316));
			} else {
				LOG.error(String
						.format("We are trying to retrieve a bank account from a GET request on %s,\n but we got status code %s, %s with the following content %s ",
								path, response.getStatusInfo().getStatusCode(), response
										.getStatusInfo().getReasonPhrase(), response
										.readEntity(String.class)));
				fail(String.format("The status was not 200 but %s, %s", response.getStatusInfo()
						.getStatusCode(), response.getStatusInfo().getReasonPhrase()));
			}

		} finally {
			response.close();
		}
	}

	@Test
	public void testGetBankAccounts() throws JsonProcessingException {
		final String path = "/bank-accounts/";
		final Response response = target(path).request(MediaType.APPLICATION_JSON_TYPE).get();
		LOG.debug("HTTP Status code {}", response.getStatus());
		LOG.debug(response.getStatusInfo().getReasonPhrase());
		try {
			if (Response.Status.OK.getStatusCode() == response.getStatus()) {
				
				// To be able to read information from the response more than
				// once to get a different format
				// response.bufferEntity();
				List<BankAccount> bankAccounts = response
						.readEntity(new GenericType<List<BankAccount>>() {
						});
				assertNotNull(bankAccounts);
				assertTrue(
						"The sql insert script provides 8 bank_account records (but more could be inserted afterward)",
						bankAccounts.size() == 8);
				assertNull("Balance info is not included when retrieving all bankAccounts",
						bankAccounts.get(0).getBalance());
				assertNotNull(
						"The BankAccount should contain a Customer (with a subset of its properties available)",
						bankAccounts.get(0).getCustomer());
				assertNotNull(
						"The BankAccount should contain a Customer, with his/her firstName present",
						bankAccounts.get(0).getCustomer().getFirstName());
				assertNull(
						"The BankAccount should contain a Customer, without his/her transitive accounts property",
						bankAccounts.get(0).getCustomer().getAccounts());
				LOG.debug(String
						.format("We were able to retrieve a bank account from a GET request on %s\n with the following content\n %s ",
								path, bankAccounts));
			} else {
				LOG.error(String
						.format("We trying to retrieve a bank account from a GET request on %s,\n but we got status code %s, %s with the following content %s ",
								path, response.getStatusInfo().getStatusCode(), response
										.getStatusInfo().getReasonPhrase(), response
										.readEntity(String.class)));
				fail(String.format("The status was not 200 but %s, %s", response.getStatusInfo()
						.getStatusCode(), response.getStatusInfo().getReasonPhrase()));
			}

		} finally {
			response.close();
		}
	}
}
