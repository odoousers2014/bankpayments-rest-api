<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>nl.vea.java-course</groupId>
	<artifactId>bank-payments-eai</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>war</packaging>
	<description>Soap based webservice translation of the rest-api</description>
	<properties>
		<java.se.version>1.7</java.se.version>
		<mvn.plugins.compiler.version>3.2</mvn.plugins.compiler.version>
		<mvn.plugins.war.version>2.5</mvn.plugins.war.version>


		<spring.version>4.0.3.RELEASE</spring.version>
		<spring.security.version>3.2.5.RELEASE</spring.security.version>

		<cxf.version>3.1.1</cxf.version>

		<cxf.generated.sources.directory>${project.basedir}/src/cxf-generated-source/java</cxf.generated.sources.directory>

		<cxf.wsdl.location>${project.basedir}/src/main/webapp/WEB-INF/wsdl/bank-payments-webservices.wsdl</cxf.wsdl.location>
		<cxf.binding.location>${project.basedir}/src/main/webapp/WEB-INF/wsdl/bank-payments-jaxb.xjb</cxf.binding.location>
		<!-- We like to deviate from the default ${project.basedir}/src/main/xjb -->
		<jaxb.xjb.directory>${project.basedir}/src/main/resources/xjb</jaxb.xjb.directory>

		<jersey.version>2.13</jersey.version>
		<jackson.version>2.4.3</jackson.version>

		<jetty.version>9.0.7.v20131107</jetty.version> <!-- Adapt this to a version found on http://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-maven-plugin/ -->
		<logback.version>1.1.1</logback.version>
		<jcloverslf4j.version>1.7.6</jcloverslf4j.version>
	</properties>
	<dependencyManagement>
		<dependencies>
			<!-- Spring 4 dependencies -->
			<!-- I introduced the bom after adding spring security 3.2.5.RELEASE and 
				getting deployment exceptions with root cause: java.lang.NoSuchMethodError 
				where spring security had transitive dependencies to a Spring 3 version whilst 
				we are already using Spring 4 Adding this bom repairs this problem see also: 
				http://stackoverflow.com/questions/20821155/maven-spring-4-spring-security 
				http://docs.spring.io/spring-security/site/docs/3.2.5.RELEASE/reference/htmlsingle/#maven-bom -->
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-framework-bom</artifactId>
				<version>${spring.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>

	</dependencyManagement>
	<dependencies>
		<!-- http://stackoverflow.com/questions/8330414/java-lang-illegalstateexception-applicationeventmulticaster-not-initialized -->
		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib</artifactId>
			<version>3.1</version>
		</dependency>
		<dependency>
			<groupId>javax.inject</groupId>
			<artifactId>javax.inject</artifactId>
			<version>1</version>
		</dependency>

		<!-- also in hr-poc -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.ws</groupId>
			<artifactId>spring-ws-core</artifactId>
			<version>2.2.1.RELEASE</version>
		</dependency>
		<!-- LogBack dependencies -->
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${jcloverslf4j.version}</version>
		</dependency>

		<!--Apache CXF JAX-WS implementation -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>${cxf.version}</version>
		</dependency>

		<!--Apache CXF JAX-RS client implementation -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-client</artifactId>
			<version>${cxf.version}</version>
		</dependency>

		<!-- jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-base</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<version>${jackson.version}</version>
		</dependency>


		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
			<scope>provided</scope>
		</dependency>


		<!-- Jersey-client -->
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-client</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
			<version>${jersey.version}</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<!-- Willem: plugin added to force java se version to 1.7 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${mvn.plugins.compiler.version}</version>
				<configuration>
					<source>${java.se.version}</source>
					<target>${java.se.version}</target>
					<!-- disable annotation processing for the compile plugin because of 
						conflicts with the JPAMetaModelEntityProcessor -->
					<compilerArgument>-proc:none</compilerArgument>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-codegen-plugin</artifactId>
				<version>${cxf.version}</version>
				<executions>
					<execution>
						<id>generate-sources</id>
						<phase>generate-sources</phase>
						<configuration>
							<defaultOptions>
								<bindingFiles>
									<bindingFile>${cxf.binding.location}</bindingFile>
								</bindingFiles>
								<noAddressBinding>true</noAddressBinding>
							</defaultOptions>
							<sourceRoot>${cxf.generated.sources.directory}</sourceRoot>
							<wsdlOptions>
								<wsdlOption>
									<wsdl>${cxf.wsdl.location}</wsdl>
								</wsdlOption>
							</wsdlOptions>
						</configuration>
						<goals>
							<goal>wsdl2java</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>