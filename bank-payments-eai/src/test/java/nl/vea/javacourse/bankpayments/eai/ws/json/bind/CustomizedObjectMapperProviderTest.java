package nl.vea.javacourse.bankpayments.eai.ws.json.bind;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.math.BigDecimal;

import nl.vea.javacourse.bankpayments.eai.ws.gen.BankAccount;
import nl.vea.javacourse.bankpayments.eai.ws.gen.Customer;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain.CustomerMixIn;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CustomizedObjectMapperProviderTest {


	@Test
	public void testMixInPresent()  {

		assertEquals(2, CustomizedObjectMapperProvider
				.getCustomizedObjectMapper().mixInCount());

		@SuppressWarnings("rawtypes")
		Class mixInForCustomerClass = CustomizedObjectMapperProvider
				.getCustomizedObjectMapper()
				.findMixInClassFor(Customer.class);
		assertEquals(CustomerMixIn.class.getName(),
				mixInForCustomerClass.getName());
	}
	
	@Test
	public void testUnmarshalling() throws JsonParseException, JsonMappingException, IOException{
		BankAccount bankAccount = CustomizedObjectMapperProvider
				.getCustomizedObjectMapper()
				.readValue(
						Thread.currentThread()
								.getContextClassLoader()
								.getResource(
										"nl/vea/javacourse/bankpayments/eai/ws/json/bind/bankAccount_312631316.json"),
						BankAccount.class);
		assertNotNull(bankAccount);
		assertEquals(312631316, bankAccount.getAccountNumber().intValue());
		assertEquals(BigDecimal.valueOf(-316.55), bankAccount.getBalance());
		
		Customer willem = bankAccount.getCustomer();
		assertNotNull(willem);
		assertEquals("Willem", willem.getFirstName());
	}
	
	@Test
	public void testCopyMixInPresent(){
		assertEquals(2, CustomizedObjectMapperProvider
				.getCustomizedObjectMapperCopy().mixInCount());

		@SuppressWarnings("rawtypes")
		Class mixInForCustomerClass = CustomizedObjectMapperProvider
				.getCustomizedObjectMapperCopy()
				.findMixInClassFor(Customer.class);
		assertEquals(CustomerMixIn.class.getName(),
				mixInForCustomerClass.getName());
	}
	
	@Test
	public void testCopyUnmarshalling() throws JsonParseException, JsonMappingException, IOException{
		BankAccount bankAccount = CustomizedObjectMapperProvider
				.getCustomizedObjectMapperCopy()
				.readValue(
						Thread.currentThread()
								.getContextClassLoader()
								.getResource(
										"nl/vea/javacourse/bankpayments/eai/ws/json/bind/bankAccount_312631316.json"),
						BankAccount.class);
		assertNotNull(bankAccount);
		assertEquals(312631316, bankAccount.getAccountNumber().intValue());
		assertEquals(BigDecimal.valueOf(-316.55), bankAccount.getBalance());
		
		Customer willem = bankAccount.getCustomer();
		assertNotNull(willem);
		assertEquals("Willem", willem.getFirstName());
	}

}
