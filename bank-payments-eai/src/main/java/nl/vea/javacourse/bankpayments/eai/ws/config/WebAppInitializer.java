package nl.vea.javacourse.bankpayments.eai.ws.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
//import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
//import org.springframework.ws.transport.http.MessageDispatcherServlet;

public class WebAppInitializer implements WebApplicationInitializer {

	// @Override
	// public void onStartup(ServletContext servletContext)
	// throws ServletException {
	// AnnotationConfigWebApplicationContext rootContext = new
	// AnnotationConfigWebApplicationContext();
	// rootContext.register(AnnotationConfigWebApplicationContext.class);
	// servletContext.addListener(new ContextLoaderListener(rootContext));
	// ServletRegistration.Dynamic dispatcher =
	// servletContext.addServlet("spring-ws", MessageDispatcherServlet.class);
	// dispatcher.addMapping("/*");
	//
	// }

	// For CXF which didn't work
	// @Override
	// public void onStartup(ServletContext servletContext)
	// throws ServletException {
	// AnnotationConfigWebApplicationContext rootContext = new
	// AnnotationConfigWebApplicationContext();
	// rootContext.register(AnnotationConfigWebApplicationContext.class);
	// servletContext.addListener(new ContextLoaderListener(rootContext));
	// ServletRegistration.Dynamic dispatcher =
	// servletContext.addServlet("CXFServlet", CXFServlet.class);
	// dispatcher.setLoadOnStartup(0);
	// dispatcher.addMapping("/*");
	// }

	public void onStartup(ServletContext container) throws ServletException {
		XmlWebApplicationContext context = new XmlWebApplicationContext();
		context.setConfigLocation("classpath:spring/cxfSpringContext.xml");
		container.addListener(new ContextLoaderListener(context));
		ServletRegistration.Dynamic cxf = container.addServlet("cxf",
				new CXFServlet());
		cxf.setLoadOnStartup(1);
		cxf.addMapping("/*");
	}

}
