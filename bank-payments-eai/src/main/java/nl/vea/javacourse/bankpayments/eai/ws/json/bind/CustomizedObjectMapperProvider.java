package nl.vea.javacourse.bankpayments.eai.ws.json.bind;

import java.text.SimpleDateFormat;

import javax.ws.rs.client.Client;
//import javax.ws.rs.ext.ContextResolver;
//import javax.ws.rs.ext.Provider;



import nl.vea.javacourse.bankpayments.eai.ws.gen.BankAccount;
import nl.vea.javacourse.bankpayments.eai.ws.gen.Customer;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain.BankAccountMixIn;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain.CustomerMixIn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

//import org.codehaus.jackson.map.SerializationConfig.Feature;

/**
 * Configures a single customized instance of {@link ObjectMapper}, configured
 * to add MixIn annotations to unmarshall JSON messages of our
 * bank-payments-rest-jersey-xml application to the entities of this
 * bank-payments-eai soap services generated from the wsdl. Does this provider
 * annotation works on the client side? or do we register this with the
 * {@link Client} object in another way?
 * 
 * @author Willem
 *
 */

// @Provider
public class CustomizedObjectMapperProvider /*
											 * implements
											 * ContextResolver<ObjectMapper>
											 */{
	private static final Logger LOG = LoggerFactory
			.getLogger(CustomizedObjectMapperProvider.class);
	
	//ISO-8601 Date-Timestamp
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	private static final ObjectMapper customizedObjectMapper;

	static {
		customizedObjectMapper = createCustomizedMapper();
	}

	public CustomizedObjectMapperProvider() {
		super();
		LOG.debug("The constructor of CustomizedObjectMapperProvider is called");
	}

	// @Override
	// public ObjectMapper getContext(Class<?> type) {
	// // TODO Auto-generated method stub
	// return defaultObjectMapper;
	// }

	private static ObjectMapper createCustomizedMapper() {
		LOG.debug("Creating the customized ObjectMapper for parsing JSON");
		final ObjectMapper mapper = new ObjectMapper();

		// https://github.com/FasterXML/jackson-databind#10-minute-tutorial-configuration
		// to prevent exception when encountering unknown property:
		// This would make additional mixIn superfluous in this case
		// commented out to test working of the mixIn interfaces
		// /mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		// Configure date format used for serializing Date objects
		final SimpleDateFormat defaultDateFormat = new SimpleDateFormat(
				DEFAULT_DATE_FORMAT);
		mapper.setDateFormat(defaultDateFormat);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		//mapper.disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS);
		// Provides Mixin functionality
		mapper.addMixInAnnotations(BankAccount.class, BankAccountMixIn.class);
		mapper.addMixInAnnotations(Customer.class, CustomerMixIn.class);

		return mapper;
	}

	/**
	 * Provides the singleton instance of the customized {@link ObjectMapper}.
	 * This is a heavy weight thread-safe object.
	 * 
	 * @return
	 */
	public static ObjectMapper getCustomizedObjectMapper() {
		return customizedObjectMapper;
	}

	/**
	 * Provides a copy of the customized instance of {@link ObjectMapper}, with
	 * the same configuration settings. This would be necessary when you want to
	 * alter configurations as this can not be done reliably on a mapper once it
	 * has been involved in reading or writing actual objects.
	 * 
	 * @return
	 */
	public static ObjectMapper getCustomizedObjectMapperCopy() {
		return customizedObjectMapper.copy();
	}

}
