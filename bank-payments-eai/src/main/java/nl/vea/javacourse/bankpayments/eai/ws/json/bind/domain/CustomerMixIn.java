package nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain;

import nl.vea.javacourse.bankpayments.eai.ws.gen.Customer;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.CustomizedObjectMapperProvider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Jackson annotation MixIn interface to add any required Jackson annotations to
 * the target class. The Target class {@link Customer} is generated from a wsdl,
 * therefore this loosely annotation application is very convenient.
 * {@link CustomizedObjectMapperProvider} is responsible for binding this MixIn
 * interface to the target class.
 * 
 * @author Willem
 *
 */
// This annotation results in just ignoring all properties in a JSON message
// that cannot be mapped to any of the properties of the target class
@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonIgnoreProperties({ "accountUserName", "accounts" })
public interface CustomerMixIn {
	// Besides ignoring properties not present in the target class all property
	// names are identical so no additional annotations are necessary.
}
