package nl.vea.javacourse.bankpayments.eai.ws.config;

//import org.apache.cxf.transport.servlet.CXFServlet;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.xml.ws.Endpoint;

import nl.vea.javacourse.bankpayments.eai.ws.BankPaymentsWebServiceEndpoint;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.CustomizedObjectMapperProvider;

import org.apache.cxf.jaxws.EndpointImpl;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
@ComponentScan(basePackages = { /* "org.apache.cxf", */
"nl.vea.javacourse.bankpayments.eai" })
// @ImportResource({ "classpath:META-INF/cxf/cxf-servlet.xml",
// "classpath:META-INF/cxf/cxf.xml" })
public class SpringApplicationContext {

	private static final Logger LOG = LoggerFactory
			.getLogger(SpringApplicationContext.class);

	@Autowired
	private BankPaymentsWebServiceEndpoint bankPaymentsEndpoint;

	@Bean
	public Endpoint bankPaymentsEndpoint() {
		LOG.info("Publishing the BankPaymentsWebServiceEndpoint");
		EndpointImpl endpoint = new EndpointImpl(bankPaymentsEndpoint);
		endpoint.publish("/bank-payments");
		return endpoint;
	}

	@Bean
	public Client jaxrsClient() {
		//is it possible to set the username password per {@link javax.ws.rs.client.WebTarget}
		//to relay it from the HTTP header of the soap request
		//Actually a Jersey feature which we seem to be able to combine with a CXF JAX-RS client runtime
		//If we faile to register this basic authentication we get a status 401 REST response
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(
				"admin", "admin");
		JacksonJsonProvider jsonProvider = new JacksonJsonProvider();
		ObjectMapper mapper = CustomizedObjectMapperProvider.getCustomizedObjectMapper();
		jsonProvider.setMapper(mapper);
		Client client = ClientBuilder.newBuilder().register(jsonProvider)
				.register(feature).build();
		LOG.debug(String.format("jsonProvider is registered: %s", client.getConfiguration().isRegistered(jsonProvider)));
		LOG.debug(String.format("mixin count: %s", mapper.mixInCount()));
		return client;
	}

}
