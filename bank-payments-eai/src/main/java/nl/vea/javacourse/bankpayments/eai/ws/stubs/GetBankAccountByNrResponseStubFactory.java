package nl.vea.javacourse.bankpayments.eai.ws.stubs;

import java.math.BigDecimal;

import nl.vea.javacourse.bankpayments.eai.ws.gen.BankAccount;
import nl.vea.javacourse.bankpayments.eai.ws.gen.Customer;
import nl.vea.javacourse.bankpayments.eai.ws.gen.GetBankAccountByNrRequest;
import nl.vea.javacourse.bankpayments.eai.ws.gen.GetBankAccountByNrResponse;

public class GetBankAccountByNrResponseStubFactory {
	public static GetBankAccountByNrResponse createStub(GetBankAccountByNrRequest request){
		Customer willem = new Customer();
		willem.setCustomerId(1);
		willem.setFirstName("Willem");
		willem.setNameInsertion("van");
		willem.setLastName("Es");
		BankAccount account = new BankAccount();
		account.setCustomer(willem);
		account.setAccountId(1);
		account.setAccountNumber(request.getAccountNumber());
		account.setBalance(BigDecimal.valueOf(946.57));
		account.setCreditLimit(BigDecimal.valueOf(1000L));
		GetBankAccountByNrResponse response = new GetBankAccountByNrResponse();
		response.setBankAccount(account);
		return response;
	}
}
