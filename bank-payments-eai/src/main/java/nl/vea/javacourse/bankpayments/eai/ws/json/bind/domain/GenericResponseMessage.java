package nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//This annotation results in just ignoring all properties in a JSON message
//that cannot be mapped to any of the properties of the target class
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericResponseMessage implements Serializable{
	
	private static final long serialVersionUID = 7293051482439688936L;

	/** contains the same HTTP Status code returned by the server */
	private int statusCode;	
	
	/** contains the  HTTP Status name returned by the server */
	private String statusName;
	
	private String message;
	
	public GenericResponseMessage(){
		super();
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public String getMessage() {
		return message;
	}

	public void setStatusCode(final int statusCode) {
		this.statusCode = statusCode;
	}

	public void setStatusName(final String statusName) {
		this.statusName = statusName;
	}

	public void setMessage(final String message) {
		this.message = message;
	}
	
}
