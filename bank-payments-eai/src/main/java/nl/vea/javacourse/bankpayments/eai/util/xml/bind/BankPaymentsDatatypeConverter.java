package nl.vea.javacourse.bankpayments.eai.util.xml.bind;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.xml.bind.DatatypeConverter;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;


public class BankPaymentsDatatypeConverter  {

	public String parseAnySimpleType(String lexicalXSDAnySimpleType) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseAnySimpleType(lexicalXSDAnySimpleType);
	}

	public byte[] parseBase64Binary(String lexicalXSDBase64Binary) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseBase64Binary(lexicalXSDBase64Binary);
	}


	public boolean parseBoolean(String lexicalXSDBoolean) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseBoolean(lexicalXSDBoolean);
	}


	public byte parseByte(String lexicalXSDByte) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseByte(lexicalXSDByte);
	}


	public Calendar parseDate(String lexicalXSDDate) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseDate(lexicalXSDDate);
	}


	public Calendar parseDateTime(String lexicalXSDDateTime) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseDateTime(lexicalXSDDateTime);
	}


	public BigDecimal parseDecimal(String lexicalXSDDecimal) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseDecimal(lexicalXSDDecimal);
	}


	public double parseDouble(String lexicalXSDDouble) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseDouble(lexicalXSDDouble);
	}


	public float parseFloat(String lexicalXSDFloat) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseFloat(lexicalXSDFloat);
	}


	public byte[] parseHexBinary(String lexicalXSDHexBinary) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseHexBinary(lexicalXSDHexBinary);
	}


	public static int parseInt(String lexicalXSDInt) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseInt(lexicalXSDInt);
	}


	public static Integer parseInteger(String lexicalXSDInteger) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseInteger(lexicalXSDInteger).intValue();
	}


	public long parseLong(String lexicalXSDLong) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseLong(lexicalXSDLong);
	}


	public QName parseQName(String lexicalXSDQName, NamespaceContext nsc) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseQName(lexicalXSDQName, nsc);
	}


	public short parseShort(String lexicalXSDShort) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseShort(lexicalXSDShort);
	}


	public String parseString(String lexicalXSDString) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseString(lexicalXSDString);
	}


	public Calendar parseTime(String lexicalXSDTime) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseTime(lexicalXSDTime);
	}


	public long parseUnsignedInt(String lexicalXSDUnsignedInt) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseUnsignedInt(lexicalXSDUnsignedInt);
	}


	public int parseUnsignedShort(String lexicalXSDUnsignedShort) {
		// TODO Auto-generated method stub
		return DatatypeConverter.parseUnsignedShort(lexicalXSDUnsignedShort);
	}


	public String printAnySimpleType(String val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printAnySimpleType(val);
	}


	public String printBase64Binary(byte[] val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printBase64Binary(val);
	}


	public String printBoolean(boolean val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printBoolean(val);
	}


	public String printByte(byte val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printByte(val);
	}


	public String printDate(Calendar val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printDate(val);
	}


	public String printDateTime(Calendar val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printDateTime(val);
	}


	public String printDecimal(BigDecimal val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printDouble(double val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printFloat(float val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printHexBinary(byte[] val) {
		// TODO Auto-generated method stub
		return null;
	}


	public static String printInt(int val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printInt(val);
	}


	public static String printInteger(Integer val) {
		// TODO Auto-generated method stub
		return DatatypeConverter.printInt(val);
	}


	public String printLong(long val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printQName(QName arg0, NamespaceContext arg1) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printShort(short val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printString(String val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printTime(Calendar val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printUnsignedInt(long val) {
		// TODO Auto-generated method stub
		return null;
	}


	public String printUnsignedShort(int val) {
		// TODO Auto-generated method stub
		return null;
	}

}
