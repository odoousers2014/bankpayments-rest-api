package nl.vea.javacourse.bankpayments.eai.ws;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.Holder;

import nl.ing.lending.kbm.eai.base.exceptions._2013._07.EaiSoapFaultType;
import nl.ing.lending.kbm.eai.base.header._2013._07.EaiHeader;
import nl.vea.java_course.bank_payments.eai.webservice._2015._05.BankPaymentsServicePort;
import nl.vea.java_course.bank_payments.eai.webservice._2015._05.EaiSoapFault;
import nl.vea.javacourse.bankpayments.eai.ws.gen.BankAccount;
import nl.vea.javacourse.bankpayments.eai.ws.gen.BankAccounts;
import nl.vea.javacourse.bankpayments.eai.ws.gen.GetBankAccountByNrRequest;
import nl.vea.javacourse.bankpayments.eai.ws.gen.GetBankAccountByNrResponse;
import nl.vea.javacourse.bankpayments.eai.ws.gen.GetBankAccountsResponse;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.CustomizedObjectMapperProvider;
import nl.vea.javacourse.bankpayments.eai.ws.json.bind.domain.GenericResponseMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

@Component
// @WebService(endpointInterface =
// "nl.vea.java_course.bank_payments.eai.webservice._2015._05.BankPaymentsServicePort",
// targetNamespace =
// "http://java-course.vea.nl/bank-payments/eai/webservice/2015/05",
// serviceName = "BankPaymentsService",
// portName = "BankPaymentsServiceSoapBinding",
// wsdlLocation = "WEB-INF/wsdl/bank-payments-webservices.wsdl")
public class BankPaymentsWebServiceEndpoint implements BankPaymentsServicePort {

	private static final Logger LOG = LoggerFactory
			.getLogger(BankPaymentsWebServiceEndpoint.class);

	private static final String URL_PARAM_ACCOUNT_NR = "account-nr";
	/**
	 * TODO the hostname and port number part of the url should be configurable
	 * by a property file
	 */
	private static final String BANK_ACCOUNT_BY_ACCOUNT_NR_URL = "https://localhost:8443/bank-payments-rest-jersey-xml/bank-accounts/account-nr/{"
			+ URL_PARAM_ACCOUNT_NR + "}";
	
	private static final String BANK_ACCOUNTS_URL = "https://localhost:8443/bank-payments-rest-jersey-xml/bank-accounts/";

	@Autowired
	Client restClient;

	@Override
	// @WebMethod(operationName="getBankAccountByNr")
	public GetBankAccountByNrResponse getBankAccountByNr(
			Holder<EaiHeader> header, GetBankAccountByNrRequest body)
			throws EaiSoapFault {
		LOG.debug(String.format("Calling the service with account nr %s",
				body.getAccountNumber()));
		try {
			return delegateAccountByNrToREST(header, body);
		} catch (JsonProcessingException e) {
			LOG.error("Processing the JSON response went wrong.", e);
			EaiSoapFaultType faultType = new EaiSoapFaultType();
			faultType.setErrorId("500");
			// faultType.set(message.getStatusName());
			throw new EaiSoapFault("Processing the JSON response went wrong.",
					faultType);
		}
		// return GetBankAccountByNrResponseStubFactory.createStub(body);

	}

	private GetBankAccountByNrResponse delegateAccountByNrToREST(
			final Holder<EaiHeader> header, final GetBankAccountByNrRequest body)
			throws JsonProcessingException, EaiSoapFault {
		Response response = null;
		try {
			response = restClient
					.target(BANK_ACCOUNT_BY_ACCOUNT_NR_URL)
					.resolveTemplate(URL_PARAM_ACCOUNT_NR,
							body.getAccountNumber())
					.request(MediaType.APPLICATION_JSON_TYPE).get();

			LOG.debug(String.format("Response status %s", response.getStatus()));
			if (response.getStatus() == 200) {
				BankAccount account = response.readEntity(BankAccount.class);
				LOG.debug(CustomizedObjectMapperProvider
						.getCustomizedObjectMapper()
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(account));
				GetBankAccountByNrResponse soapResponse = new GetBankAccountByNrResponse();
				soapResponse.setBankAccount(account);
				return soapResponse;
			} else {
				GenericResponseMessage message = response
						.readEntity(GenericResponseMessage.class);
				LOG.warn(CustomizedObjectMapperProvider
						.getCustomizedObjectMapper()
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(message));
				LOG.warn(String.format("Response status %s",
						response.getStatus()));
				LOG.warn(String.format("Response status info %s",
						response.getStatusInfo()));
				EaiSoapFaultType faultType = new EaiSoapFaultType();
				faultType.setErrorId(Integer.toString(message.getStatusCode()));
				// faultType.set(message.getStatusName());
				throw new EaiSoapFault(message.getMessage(), faultType);
				// TODO propably throw an exception
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public GetBankAccountsResponse getBankAccounts(Holder<EaiHeader> header,
			Object body) throws EaiSoapFault {
		LOG.debug("Calling the getAccounts service.");
		try {
			return delegateAccountsToREST(header, body);
		} catch (JsonProcessingException e) {
			LOG.error("Processing the JSON response went wrong.", e);
			EaiSoapFaultType faultType = new EaiSoapFaultType();
			faultType.setErrorId("500");
			// faultType.set(message.getStatusName());
			throw new EaiSoapFault("Processing the JSON response went wrong.",
					faultType);
		}
	}
	
	private GetBankAccountsResponse delegateAccountsToREST(
			final Holder<EaiHeader> header, final Object body)
			throws JsonProcessingException, EaiSoapFault {
		Response response = null;
		try {
			response = restClient
					.target(BANK_ACCOUNTS_URL)
					.request(MediaType.APPLICATION_JSON_TYPE).get();

			LOG.debug(String.format("Response status %s", response.getStatus()));
			if (response.getStatus() == 200) {
				List<BankAccount> accountList = response.readEntity(new GenericType<List<BankAccount>>(){});
				LOG.debug(CustomizedObjectMapperProvider
						.getCustomizedObjectMapper()
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(accountList));
//				BankAccounts accounts = new BankAccounts();
//				accounts.getBankAccount().addAll(accountList);
				GetBankAccountsResponse soapResponse = new GetBankAccountsResponse();
				soapResponse.setBankAccounts(accountList);
				return soapResponse;
			} else {
				GenericResponseMessage message = response
						.readEntity(GenericResponseMessage.class);
				LOG.warn(CustomizedObjectMapperProvider
						.getCustomizedObjectMapper()
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(message));
				LOG.warn(String.format("Response status %s",
						response.getStatus()));
				LOG.warn(String.format("Response status info %s",
						response.getStatusInfo()));
				EaiSoapFaultType faultType = new EaiSoapFaultType();
				faultType.setErrorId(Integer.toString(message.getStatusCode()));
				// faultType.set(message.getStatusName());
				throw new EaiSoapFault(message.getMessage(), faultType);
				// TODO propably throw an exception
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}
}
